<?php
	require_once("utilities/config.php");
	require_once("utilities/lib.php");
	session_start();
	session_regenerate_id(true);
	try {
    // MySQLサーバへ接続
   	$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e){
    		die($e->getMessage());
	}
	$stmt = $pdo->prepare("SELECT * FROM `role_tbl` INNER JOIN `dr_tbl` ON `role_tbl`.`dr_tbl_id` = `dr_tbl`.`id` WHERE 1=1;");
	$stmt->execute();
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$total_no = $stmt->rowCount();
	
	$html = '<html><head>
	<style type="text/css">
div#main {
	width:600px;
	margin-left:auto;
	margin-right:auto;
	background-color:grey;
	color:white;
	font-weight:bold;
}
table#tbl {
	border:2px white solid;
}
td{
	color:yellow;
	border:2px red solid;
	font-weight:bold;
}
td.td {
	color:yellow;
	font-weight:bold;
}
</style></head>
	<body><div id="main">';
	
	foreach($rows as $row) {
		$html .= '<table id="tbl"><tr>
	<td>氏名 : </td><td>'._Q($row['dr_name']).'</td></tr>
    <tr><td>アルファベット名前 :  </td><td>'._Q($row['dr_name_alpha']).'</td></tr>     
 	<tr><td>性別 : </td><td>';
		if ($row['is_male'] == 1) $html = $html."男性";
		else $html = $html."女性";
    $html = $html.'</td></tr>
	<tr><td>生まれた年 : </td><td>'.date('Y', strtotime($row['birth_year'])).'年</td></tr>
	<tr><td>所属病院/所属会社/所属団体名 : </td>
    <td>'._Q($row['hp_name']).'</td></tr>	
	<tr><td>職種 : </td><td>'.$job_kinds[$row['job_kind']].'</td></tr>
	<tr><td class="td">電子メール・アドレス : </td><td class="td">'._Q($row['email']).'</td></tr>
	<tr><td>演題名 : </td><td>'._Q($row['topic_title']).'</td></tr>
	<tr><td>内容 : </td><td>'._Q($row['topic_abstract']).'</td></tr>
	</table><br />';
	}
$html .= '</div></body></html>';	
	
require_once 'mpdf/mpdf.php';

//$html = file_get_contents("http://www.yahoo.co.jp");
//$html = preg_replace('/(<body.*?)>/', '$1 lang="ja">;', $html);

$mpdf = new mPDF('ja', 'A4');
$mpdf->useAdobeCJK = true;
$mpdf->WriteHTML($html);
$mpdf->Output();
exit(0);

?>
