<?php		// member_registration02.php

	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');	
	charSetUTF8();
//	session_set_cookie_params(0, "/", "/member/", TRUE, TRUE);
	session_start();
	$today_year = date("Y");
	$_SESSION['auth_forget'] = hash("sha512", $magic_code.$_POST['email']);
	
	if (!isset($_SESSION['forget_flag'])||($_SESSION['forget_flag'] == FALSE)) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}
	
 	//入力エラーのチェック
	$error='';

	if (!isset($_POST['email'])||($_POST['email'] == '')) $error.='電子メール・アドレスがありません<br />';
	if (strlen($_POST['email'])>64) $error.='電子メール・アドレスが長すぎます<br />';
	if (!mb_check_encoding($_POST['email'], 'UTF-8')) $error.='電子メール・アドレスに不正な文字列が混入しています<br />';
	$return = preg_match('/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/', $_POST['email']);
	if ($return == 0) $error .= '電子メール・アドレスが不正です<br />';

	if (!isset($_POST['clue'])||!is_numeric($_POST['clue'])||($_POST['clue']<1)||($_POST['clue']>4)) $error.='不正なヒント選択です<br />';
	if ($_POST['hint']=='') $error.='ヒント入力がありません<br />';
	if (!mb_check_encoding($_POST['hint'], 'UTF-8')) $error.='ヒントに不正な文字列が混入しています<br />';
	if (strlen($_POST['hint'])>30) $error.='ヒントが長すぎます<br />';

//接続
 	try {
    // MySQLサーバへ接続
   	$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e){
    	die($e->getMessage());
	}

	$stmt = $pdo->prepare("SELECT * FROM `dr_tbl` WHERE `email` = :email;");
	$stmt->bindValue(":email", $_POST['email']);
	$stmt->execute();
	$result = $stmt->fetch(PDO::FETCH_ASSOC); 

	if ($stmt->rowCount() == 0) {
		$error.='該当する電子メール・アドレスは登録されていません<b/>';
		$_SESSION['forget_flag'] = FALSE;
	} 

	//エラーのある場合
	if ($error != '') {
?>

<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<link rel="stylesheet" type="text/css" href="..?css/index.css"/>
 <script src="../javascript/jquery-1.10.2.js"></script>
<script src="../javascript/jquery-corner.js"></script>
<script src="../javascript/index.js"></script>
<title>NPO TRI</title>
</head>
<body>
	<div align="center">
	<h1>Reset Your Password</h1><br/><br/>
	<font color="red"><?=$error ?></font><br />	
	 ブラウザの「戻る」ボタンを用いて戻り、間違いを訂正して下さい
	</div>
</body>
</html>
<?php
 	exit();
 }
?>

<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<link rel="stylesheet" type="text/css" href="..?css/index.css"/>
 <script src="../javascript/jquery-1.10.2.js"></script>
<script src="../javascript/jquery-corner.js"></script>
<script src="../javascript/index.js"></script>
<title>NPO TRI</title>
</head>
<body bgcolor="#AADDFF">
<div align="center">
<h1>パスワード・リセット画面 [2/4]<br/><br/></h1>

<table>
<form action="forget03.php" method="post">
	<input type="hidden" name="email" value="<?= _Q($_POST['email']) ?>" />
    <input type="hidden" name="clue" value="<?= _Q($_POST['clue']) ?>"  />
    <input type="hidden" name="hint" value="<?= _Q($_POST['hint']) ?>" />
 
<tr>
	<tr><td>Email Address : </td><td><?= _Q($_POST['email']) ?></td></tr>
	<tr><td>Hint when forgetting password : </td><td><?= $hints[$_POST['clue']] ?></td></td>
    <tr><td>Answer for the hint : </td><td><?= _Q($_POST['hint']) ?></td></tr>
<tr>
	<td colspan="2" align="center"><input type="submit" value="- 上記情報の正確性を確認します -"></td>
</tr>
</form>
</table>
</div>
</body>
</html>