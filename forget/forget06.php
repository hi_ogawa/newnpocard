<?php		// forget06.php

 //
	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');	
	charSetUTF8();
//	session_set_cookie_params(0, "/", "/member/", TRUE, TRUE);		
	session_start();
	
	if (!isset($_SESSION['auth_forget'])||($_SESSION['auth_forget'] != hash("sha512", $magic_code))) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}
	
	$error = '';
	if (!isset($_POST['dr_pwd'])||!isset($_POST['dr_pwd2'])||($_POST['dr_pwd'] == '')||($_POST['dr_pwd2'] == '')) header('Location: forget05.php');
	if (strlen($_POST['dr_pwd'])>32) $error.='パスワードが長すぎます<br />';
	$return = preg_match('/[a-zA-Z0-9]+/', $_POST['dr_pwd']);
	if ($return == 0) $error .= '不正な文字がパスワードに使用されています<br />';
	if ($_POST['dr_pwd'] != $_POST['dr_pwd2']) $error = 'パスワードが一致しません<br />';
	
	//エラーのある場合
	if ($error != '') {
?>

<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<link rel="stylesheet" type="text/css" href="css/index.css"/>
 <script src="../javascript/jquery-1.10.2.js"></script>
<script src="../javascript/jquery-corner.js"></script>
<script src="../javascript/index.js"></script>
    <link rel="stylesheet" type="text/css" href="../mem_reg/member_registration.css"/>
 	<title>NPO TRI</title>
</head>
<body>
	<div align="center">
	<h1>パスワード・リセット画面 [2/4]</h1><br/><br/>
	<p id="error"><?=$error ?></p><br />	
	<h3>ブラウザの「戻る」ボタンを用いて戻り、間違いを訂正して下さい</h3>
	</div>
</body>
</html>
<?php
		exit();
	}
//接続
 	try {
    // MySQLサーバへ接続
   	$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e){
    	die($e->getMessage());
	}
/*	
	//接続
	$my_con = mysql_connect($db_host, $db_user, $db_password);
	if ($my_con == NULL) {
		echo "Cannot open MySQL!";
    	die("MySQL connection failed");
	}
	mysql_select_db($db_name);	
	$result = mysql_query('set character set utf8');

	$sql = "UPDATE `dr_tbl` SET `dr_pwd` = '".hash("sha512", $_POST['dr_pwd'].$magic_code)."' WHERE `id` = '".mysql_real_escape_string($_POST['dr_tbl_id'])."';";
	mysql_query($sql);
	$sql = "SELECT * FROM `dr_tbl` WHERE `id` = '".mysql_real_escape_string($_POST['dr_tbl_id'])."';";
	$result = mysql_query($sql);
	$row = mysql_fetch_assoc($result);
*/
	$stmt = $pdo->prepare("UPDATE `dr_tbl` SET `dr_pwd` = :dr_pwd WHERE `id` = :dr_tbl_id;");
	$stmt->bindValue(":dr_pwd", hash("sha512", $_POST['dr_pwd'].$magic_code));
	$stmt->bindValue(":dr_tbl_id", $_POST['dr_tbl_id']);
	$stmt->execute();
	$stmt = $pdo->prepare("SELECT * FROM `dr_tbl` WHERE `id` = :dr_tbl_id;");
	$stmt->bindValue(":dr_tbl_id", $_POST['dr_tbl_id']);
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);	
// メールでパスワード変更を知らせる
		$subject = "${site_name} よりのお知らせ";
		$sender = mb_encode_mimeheader("特定非営利活動法人ティー・アール・アイ国際ネットワーク");
		$headers  = "FROM: ".$sender."<$support_mail>\r\n";		
		$parameters = '-f'.$support_mail;
	
	//　ここからメール内容
$body = <<< _EOT_
電子メール・アドレス{$row['dr_name']} 様
	
あなたが 特定非営利活動法人ティー・アール・アイ国際ネットワークに登録された
メール・アドレス({$row['email']})のパスワードが変更されました
	
ご質問やご意見ありますれば　{$support_mail} まで
お問い合わせ下さい。
===========================================================
	$site_name
	$site_url
_EOT_;
	// _EOT_　は行の最初に記述し、;の後にコメントも無し、にしないとエラーとなる	

		mb_language('uni');
		mb_internal_encoding('utf-8');
		mb_send_mail($row['email'], $subject, $body, $headers, "-f$support_mail"); 
		if ($_SERVER['SERVER_NAME'] === 'localhost') echo $body;
		$_SESSION = array();
		session_destroy();
?>
<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
	#main { width:1000px; margin-left:auto; margin-right:auto;background-color:black;}
	h1 {color:white; text-align:center;font-size:42px;}
	td {font-size:26px;font-weight:bold;}
	td.title {width:300px; text-align:left; color:white; background-color:red;}
	td.v {width 650px; text-align:left; padding-left:5px; color:red; background-color:white;}
	input {width:500px; font-size:26px; font-color:brown; font-weight:bold; background-color:yellow; border:2px solid brown; padding:3px;}
	#confirm {width:200px; margin-top:20px;font-size:36px; border: 4px solid brown; padding: 3px; background-color:yellow; color:red; text-align:center; margin-left:auto; margin-right:auto;}
</style>
	<title>NPO TRI</title>
<body bgcolor="#AADDFF">
<div id="main">
<h1>Reset Your Password</h1>

<h3>パスワードがリセットされました<br /><br /></h3>
<p><a href="../index.php">ログインする</a></p>

</div>
</body>
</html>