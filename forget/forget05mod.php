<?php		// forget05.php

 //
	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');	
	charSetUTF8();
//	mb_http_output('UTF-8');//	session_set_cookie_params(0, "/", "/member/", TRUE, TRUE);		
	session_start();
/*	
	if (!isset($_SESSION['auth_forget'])||($_SESSION['auth_forget'] != hash("sha512", $magic_code))) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}	
*/
?>

<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">



    <!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
    
    
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<title>NPO TRI</title>
</head>
<body>
<div id="main">
<h1>Reset Your Password<br/><br/></h1>
<form class="form-horizontal">
  <div class="control-group">
    <label class="control-label" for="inputEmail">Email</label>
    <div class="controls">
      <input type="text" id="inputEmail" placeholder="Email">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputPassword">Password</label>
    <div class="controls">
      <input type="password" id="inputPassword" placeholder="Password">
    </div>
  </div>
  <div class="control-group">
    <div class="controls">
      <label class="checkbox">
        <input type="checkbox"> Remember me
      </label>
      <button type="submit" class="btn">Sign in</button>
    </div>
    <input type="text" placeholder="Text input">
  </div>
</form>
<table>
<form action="forget06.php" method="post">
	<input type="hidden" name="dr_tbl_id" value="<?= $_SESSION['dr_tbl_id'] ?>">
	
    <tr><td>Your fullname in your language : </td>
    <td><?= _Q($_SESSION['dr_name']);?></td></tr>
    <tr><td>Your alphabetical name : </td>
    <td><?= _Q($_SESSION['sirname'])." "._Q($_SESSION['firstname']); ?></td></tr>     
	<tr><td>Affiliation : </td>
    <td><?= _Q($_SESSION['hp_name']); ?></td></tr>
	<tr><td>Email address : </td>
    <td><?= _Q($_SESSION['email']); ?></td></tr>

	<tr><td>New password : </td><td>
    <input class="pw" type="password" id="dr_pwd" name="dr_pwd" size=30 maxlength=32></td></tr>
	<tr><td>Retype new password : </td><td>
    <input class="pw" type="password" id="dr_pwd2" name="dr_pwd2" size=30 maxlength=32></td></tr>

<tr><td colspan="2" align="center"><input type="submit" value="- Confirm -"></td></tr>
</form>
</table>
</div>
    <!-- Bootstrap core javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script> -->
<?php
	if ($_SERVER['HTTP_HOST'] == 'localhost') {
		echo '<script src="../javascript/jquery-1.10.2.js"></script>';
	} else {
		echo '<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>';
	}
?>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../bootstrap/docs-assets/javascript/top.js"></script>
</body>
</html>