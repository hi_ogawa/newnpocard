<?php  // initialize.php

	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');
	charSetUTF8();
	session_start();

	if (!isset($_SESSION['index_key'])||($_SESSION['index_key'] != hash("sha512", $magic_code))) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}
	if (!isset($_SESSION['auth_admin'])||($_SESSION['auth_admin'] != hash("sha512", $magic_code.'facc'))) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}
	
	 //接続
	$link = mysql_connect($db_host, $db_user, $db_password);
	if (is_null($link)) {
		echo "Cannot open MySQL!";
    	die("MySQL connection failed");
	}
	mysql_select_db($db_name);
	$result = mysql_query('set character set utf8');
	
	if (isset($_POST['init'])&&($_POST['init'] == "init")) {
		$sql = "DROP TABLE `conf_link_tbl`, `conf_tbl`, `dr_tbl`, `login_log_tbl`;";
		$result = mysql_query($sql);

	$sql = "CREATE TABLE IF NOT EXISTS `dr_tbl` (".
		"`id` INT( 11 ) NOT NULL AUTO_INCREMENT,".
		"`dr_name` VARCHAR( 64 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL default '',".
		"`dr_name_alpha` VARCHAR( 64 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL default '',".
		"`is_male` BOOLEAN NOT NULL DEFAULT '1',".
		"`birth_year` DATE NOT NULL DEFAULT '0000-00-00',".
		"`hp_name` VARCHAR( 128 )  CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL default '',".
		"`job_kind` TINYINT( 2 ) NOT NULL DEFAULT '1',".
		"`email` VARCHAR( 128 ) NOT NULL DEFAULT '',".
		"`dr_pwd` VARCHAR( 64 ) NOT NULL DEFAULT '',".
		"`clue` TINYINT( 1 ) NOT NULL DEFAULT '1',".
		"`hint` VARCHAR( 64 )NOT NULL DEFAULT '',".
		"`created` DATE NOT NULL DEFAULT '0000-00-00 00:00:00',".
		"`login_date` DATE NOT NULL DEFAULT '0000-00-00 00:00:00',".
		"`ip` VARCHAR(15) NOT NULL DEFAULT '000.000.000.000',".
		"`dr_url` VARCHAR(60) NOT NULL DEFAULT '',".
		"`is_active` BOOLEAN NOT NULL DEFAULT '0',".
		"`is_usable` BOOLEAN NOT NULL DEFAULT '1',".
		"`is_deleted` BOOLEAN NOT NULL DEFAULT '0',".
		"CONSTRAINT `pk_dr_tbl` PRIMARY KEY (`id`),".
		"INDEX(`email`)".
	") ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci AUTO_INCREMENT=0;";
	mysql_query($sql);
	$sql = "CREATE TABLE IF NOT EXISTS `login_log_tbl` (".
		"`id` INT( 11 ) NOT NULL AUTO_INCREMENT,".
		"`dr_tbl_id` INT( 11 ) NOT NULL DEFAULT '0',".
		"`login_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',".
		"`login_ip` VARCHAR( 15 ) NOT NULL DEFAULT '000.000.000.000',".
		"CONSTRAINT `pk_login_log_tbl` PRIMARY KEY(`id`),".
		"INDEX(`dr_tbl_id`)".
	") ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci AUTO_INCREMENT=0;";
	mysql_query($sql);
	$sql = "CREATE TABLE IF NOT EXISTS `conf_tbl` (".
		"`id` INT( 11 ) NOT NULL AUTO_INCREMENT,".
		"`conf_link_tbl_id` INT( 11 ) NOT NULL DEFAULT '0',".
		"`begin` DATE NOT NULL DEFAULT '0000-00-00',".
		"`end` DATE NOT NULL DEFAULT '0000-00-00',".
		"`conf_jname` VARCHAR( 128 ) NOT NULL DEFAULT '',".
		"`conf_ename` VARCHAR( 128 ) NOT NULL DEFAULT '',".
		"`jplace` VARCHAR( 128 ) NOT NULL DEFAULT '',".
		"`eplace` VARCHAR( 128 ) NOT NULL DEFAULT '',".								
		"`man_size` INT( 4 ) NOT NULL DEFAULT '0',".	
		"CONSTRAINT `pk_id` PRIMARY KEY(`id`)".
	") ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci AUTO_INCREMENT=0;";
	mysql_query($sql);
	$sql = "CREATE TABLE IF NOT EXISTS `conf_link_tbl` (".
		"`id` INT( 11 ) NOT NULL AUTO_INCREMENT,".
		"`conf_tbl_id` INT( 11 ) NOT NULL DEFAULT '0',".
		"`dr_tbl_id` INT( 11 ) NOT NULL DEFAULT '0',".
		"`is_paid` BOOLEAN NOT NULL DEFAULT '0',".
		"`is_ok` BOOLEAN NOT NULL DEFAULT '0',".
		"CONSTRAINT `pk_id` PRIMARY KEY(`id`)".
	") ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci AUTO_INCREMENT=0;";
	
		mysql_query($sql);
		header('Location: ../index.php');
	}

		
 //
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="../css/index.css"/>
	<script type="text/javascript" language="javascript" src="../ajax/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" language="javascript" src="../ajax/jquery-corner.js"></script>
	<script type="text/javascript" language="javascript" src="../javascript/auth.js"></script>
<title>管理者</title>
</head>

<body>
<h1>データベースが初期化されます</h1>
<div class="center">
<form action="initialize.php" method="POST">
	<input type="hidden" id="init" name="init" value="init" />
	<input type="submit" id="submit" name="submit" value="初期化" />
    <input type="reset" id="reset" name="reset" value="キャンセル" />
</form>
</div>
</body>
</html>