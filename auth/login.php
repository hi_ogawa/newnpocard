<?php  //login.php

	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');
	charSetUTF8();
	if (isset($_POST['keep_login'])&&($_POST['keep_login'] != ''))
		session_set_cookie_params(365*24*3600, "/", "/members/", TRUE, TRUE); // 1年間ログイン継続
	else
		session_set_cookie_params(20*600, "/", "/members/", TRUE, TRUE);

	session_start();
	$_SESSION['forget_flag'] = TRUE;	// forget01.phpに飛んでも良い

//接続
 	try {
    // MySQLサーバへ接続
   	$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e){
    	die($e->getMessage());
	}

	if (isset($_POST['dr_pwd'])&&($_POST['dr_pwd'] == '')) $_POST['dr_pwd'] = time(); //これをしないと何でもOKとなってしまう

	if (isset($_POST['action'])&&($_POST['action'] == 'login')) {
		$stmt = $pdo->prepare("SELECT * FROM `dr_tbl` WHERE `email` = :email AND `is_active` = '1' AND `is_usable` = '1' AND `is_deleted` = '0';");
		// 注意: mysql_real_escape_strint()をSQL分の中で使用すると PDOでは結果が返らない!!
		$stmt->bindValue(":email", $_POST['email']);
		$stmt->execute();	
 		$users = $stmt->fetch(PDO::FETCH_ASSOC);

 		if ($users['dr_pwd'] == hash("sha512",$_POST['dr_pwd'].$magic_code)) { 

		// ログイン成功
			echo "成功<br>";
 			$_SESSION['email'] = $_POST['email'];
 			$_SESSION['dr_name'] = $users['dr_name'];
			$_SESSION['sirname'] = $users['sirname'];
			$_SESSION['firstname'] = $users['firstname'];
			$_SESSION['dr_name_alpha'] = $users['sirname']." ".$users['firstname'];
			$_SESSION['birth_year'] = $users['birth_year'];
			$_SESSION['is_male'] = $users['is_male'];
 			$_SESSION['dr_tbl_id'] = $users['id'];
			$_SESSION['job_kind'] = $users['job_kind'];
			$_SESSION['hp_name'] = $users['hp_name'];
			$_SESSION['auth_dr_code'] = hash("sha512", $magic_code.$_SESSION['email']);

			$stmt = $pdo->prepare("UPDATE dr_tbl SET login_date = :cdate WHERE email = :email;");
			$stmt->bindValue(":cdate", date('y-m-d H:i:s'));
			$stmt->bindValue("email", $_POST['email']);
			$stmt->execute();	
			$stmt = $pdo->prepare("INSERT INTO `login_log_tbl` (`dr_tbl_id`, `login_date`, `login_ip`) VALUES (:dr_tbl_id, :cdate, :ip);");

			$stmt->bindValue(":dr_tbl_id", $_SESSION['dr_tbl_id']);
			$stmt->bindValue(":cdate", date('Y-m-d H:i:s'));
			$stmt->bindValue(":ip", $_SERVER['REMOTE_ADDR']);
			$stmt->execute();

 			if (isset($_GET['redirect'])&&($_GET['redirect'] != '')) {
 				header('Location: '.$_GET['redirect']);
 				exit();
 			} else {
 				header('Location: ../index.php');
 				exit();
 			}
 		} else {
 			$message = '<font color="red"><h2>Login failed!<br>ログインに失敗しました</h2></font><br>';
			setcookie(session_name(), '', 0);
			$_SESSION = array();
			session_destroy();
				
		}
	}
?>


<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../mem_reg/member_registration.css">
<link rel="stylesheet" type="text/css" href="../css/mem_reg.css"/>

<script src="../javascript/jquery-1.10.2.js"></script>
<script  src="../javascript/jquery-corner.js"></script>
<script  src="../javascript/index1.js"></script>    
<title>Registration System of NPO TRI International Network</title>
</head>

<body>
<h1>NPO参加ログイン</h1><br />
<div id="main">
    <button class="newmember" id="new">I want to create ID (新規メンバー登録)</button>
 <?php if (isset($message)) echo $message; ?>
<form action = "login.php?redirect=<?php if (isset($_GET['redirect'])) echo urlencode($_GET['redirect']); ?>" method = "POST">
	<input type="hidden" name="action" value="login">
<table class="tbl">
 	<tr><td class="tbl">Login ID:&nbsp;</td>
 		<td class="tbl"><input type="text" name="email" size=40 maxlenght=64></td>
    </tr>
    <tr>
		<td class="tbl">Password:&nbsp;</td>
 		<td class="tbl"><input type="password" name="dr_pwd" size=30 maxlength=32></td>
	<tr>
 		<td colspan="2"><input type="checkbox" name="keep_login">Memorize password<br />次回からのためにパスワードを記憶する</td>
    </tr>
	<br />
	<tr><td colspan="2" align="center"><input class="submit" type="submit" value="Login (ログイン)"></td></tr>
	<br />
    <td colspan="2"><a href="../forget/forget01.php">I forget my password.<br />パスワード忘れましたのでパスワードをリセットします</a></td>
</table>
</form>
</div>
</body>
</html>