<?php  //login.php

	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');
	charSetUTF8();
	if (isset($_POST['keep_login'])&&($_POST['keep_login'] != ''))
		session_set_cookie_params(365*24*3600, "/", "/members/", TRUE, TRUE); // 1年間ログイン継続
	else
		session_set_cookie_params(20*600, "/", "/members/", TRUE, TRUE);

	session_start();
	$_SESSION['forget_flag'] = TRUE;	// forget01.phpに飛んでも良い

 //接続
	$link = mysql_connect($db_host, $db_user, $db_password);
	if (is_null($link)) {
		echo "Cannot open MySQL!";
    	die("MySQL connection failed");
	}
	mysql_select_db($db_name);
	$result = mysql_query('set character set utf8');
 //
	if (isset($_POST['dr_pwd'])&&($_POST['dr_pwd'] == '')) $_POST['dr_pwd'] = time(); //これをしないと何でもOKとなってしまう

	if (isset($_POST['action'])&&($_POST['action'] == 'login')) {
		$sql = "SELECT * FROM `dr_tbl` WHERE `email` = '".mysql_real_escape_string($_POST['email']).
		"' AND `is_active` = '1' AND `is_usable` = '1' AND `is_deleted` = '0';";

 		$result = mysql_query($sql);
 		$users = mysql_fetch_assoc($result);

 		if ($users['dr_pwd'] == hash("sha512", $_POST['dr_pwd'].$magic_code)) { 

		// ログイン成功
			
 			$_SESSION['email'] = $_POST['email'];
 			$_SESSION['dr_name'] = $users['dr_name'];
			$_SESSION['dr_name_alpha'] = $users['dr_name_alpha'];
			$_SESSION['birth_year'] = $users['birth_year'];
			$_SESSION['is_male'] = $users['is_male'];
 			$_SESSION['dr_tbl_id'] = $users['id'];
			$_SESSION['job_kind'] = $users['job_kind'];
			$_SESSION['hp_name'] = $users['hp_name'];
			$_SESSION['auth_dr_code'] = hash("sha512", $magic_code.$_SESSION['email']);

// 			$_SESSION['auth_dr_code'] = hash('sha256', $magic_code.$_SESSION['email'], false);
			$sql = "UPDATE dr_tbl SET login_date = '".date('y-m-d H:i:s')."' WHERE email = '".mysql_real_escape_string($_POST['email'])."'";
			$result = mysql_query($sql);
			
			$sql = "INSERT INTO `login_log_tbl` (`dr_tbl_id`, `login_date`, `login_ip`) VALUES ('".
				$_SESSION['dr_tbl_id']."', '".date('Y-m-d H:i:s')."', '".$_SERVER['REMOTE_ADDR']."'); ";				
			mysql_query($sql);		

 			if (isset($_GET['redirect'])&&($_GET['redirect'] != '')) {
 				header('Location: '.$_GET['redirect']);
 				exit();
 			} else {
 				header('Location: ../index.php');
 				exit();
 			}
 		} else {
 			$message = '<font color="red"><h2>ログインに失敗しました</h2></font><br />';
			setcookie(session_name(), '', 0);
			$_SESSION = array();
			session_destroy();
				
		}
	}
 ?>


<html><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xlmns="http://www.w3.org/1993/xhtml" xml:lang="ja" lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="../mem_reg/member_registration.css">
	<title>NPO Membership Registration [1/4]</title>
</head>

<body>
<h1>NPO参加ログイン</h1><br />
<div id="main">
 <?php if (isset($message)) echo $message; ?>
<form action = "login.php?redirect=<?php if (isset($_GET['redirect'])) echo urlencode($_GET['redirect']); ?>" method = "POST">
	<input type="hidden" name="action" value="login">
<table>
 	<tr><td>Login ID:&nbsp;</td>
 		<td><input type="text" name="email" size=40 maxlenght=64></td>
    </tr>
    <tr>
		<td>Password:&nbsp;</td>
 		<td><input type="password" name="dr_pwd" size=30 maxlength=32></td>
	<tr>
 		<td colspan="2"><input type="checkbox" name="keep_login">次回からのためにパスワードを記憶する</td>
    </tr>
	<br />
	<tr><td colspan="2" align="center"><input class="submit" type="submit" value="- ログイン -"></td></tr>
	<br />
    <td colspan="2"><a href="../forget/forget01.php">パスワード忘れましたのでパスワードをリセットします</a></td>
</table>
</form>
</div>
</body>
</html>