﻿<?
	require_once("../../../utilities/config.php");
	require_once("../../../utilities/ext_conf.php");
	require_once("../gmo_mail.php");
	class order_dao {

		private $order_unique_string;//注文番号   
		private $dr_tbl_id;//利用者ID   
		private $conf_tbl_id;//カンファランスID   
		private $pdo;
		private $hasRecordStmt;

		// order_daoの初期化
		public function setParameter($order_unique_string,$dr_tbl_id,$conf_tbl_id) 
		{
			$this->order_unique_string=$order_unique_string;
			$this->dr_tbl_id=$dr_tbl_id;
			$this->conf_tbl_id=$conf_tbl_id;

		}

		// order_daoの初期化
		public function __construct($order_unique_string,$dr_tbl_id,$conf_tbl_id) 
		{
			$this->order_unique_string=$order_unique_string;
			$this->dr_tbl_id=$dr_tbl_id;
			$this->conf_tbl_id=$conf_tbl_id;

		}

		//DBに接続する
		private function open() 
		{
		 	try
			{
				global $db_host;
				global $db_name;
				global $db_user;
				global $db_password;

				// MySQLサーバへ接続
				if($this->pdo==null)
				{
					$this->pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
					$this->pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
				}

			}
			 catch(PDOException $e)
			{
				throw $e;
			}
		return $this->pdo;
	    }

		//DBに切断する
		public function close() 
		{
		 	try
			{
				$this->hasRecordStmt=null;
				$this->pdo=null;
			}
			 catch(PDOException $e)
			{
				throw $e;
			}
	    }
		// 新規登録
		function insert($AccessID) {

			$order_tbl_id=null;

		 	try
			{
			        if (is_null($this->dr_tbl_id)==true)
				{
					throw new Exception("login info is not found");
				}


			// MySQLサーバへ接続
   			$this->pdo = $this->open();

			// トランザクションを開始、オートコミットをオフにする
   			$this->pdo->beginTransaction();

			// 注文情報のヘッダー登録
			$sqlStr="INSERT INTO order_tbl(order_unique_string,dr_tbl_id,is_ok,is_deleted,GMO_AccessID)VALUES(";
			$sqlStr=$sqlStr.":order_unique_string,";//order_unique_string
			$sqlStr=$sqlStr.":dr_tbl_id,";
			$sqlStr=$sqlStr."1,";//is_ok
			$sqlStr=$sqlStr."0,";//is_deleted
			$sqlStr=$sqlStr.":AccessID";
			$sqlStr=$sqlStr.");";


			$stmt =$this->pdo->prepare($sqlStr);
			$stmt->bindValue(":order_unique_string", $this->order_unique_string);
			$stmt->bindValue(":dr_tbl_id", intval($this->dr_tbl_id));
			$stmt->bindValue(":AccessID", $AccessID);

			$stmt->execute();
			$stmt=null;


			// 注文情報のorder_id取得
			$sqlStr="SELECT id ";
			$sqlStr=$sqlStr."FROM order_tbl ";
			$sqlStr=$sqlStr."where  ";
			$sqlStr=$sqlStr."order_unique_string=:order_unique_string";//order_unique_string
			$sqlStr=$sqlStr." and is_deleted=false";
			$stmt =$this->pdo->prepare($sqlStr);

			$stmt->bindValue(":order_unique_string", $this->order_unique_string);
			$stmt->execute();

			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

			foreach($rows as $item)
			{
				$order_tbl_id=$item['id'];
			}

			$sqlStr="SELECT ift.fee as fee, it.type as item_type, it.name as name ,it.id as item_id,dt.email as email ";
			$sqlStr=$sqlStr."FROM conf_tbl ct, ";
			$sqlStr=$sqlStr." dr_tbl dt, ";
			$sqlStr=$sqlStr." item_tbl it, ";
			$sqlStr=$sqlStr." item_fee_tbl ift ";
			$sqlStr=$sqlStr."where  ";
			$sqlStr=$sqlStr."ift.item_tbl_id = it.id "; 
			$sqlStr=$sqlStr."and dt.job_kind = ift.job_kinds_tbl_id ";
			$sqlStr=$sqlStr."and ct.item_tbl_id = it.id ";
			$sqlStr=$sqlStr."and dt.id=:dr_tbl_id ";
			$sqlStr=$sqlStr."and ct.id=:conf_tbl_id ";
			$sqlStr=$sqlStr."and ct.is_deleted=false ";
			$sqlStr=$sqlStr."and dt.is_deleted=false ";
			$sqlStr=$sqlStr."and it.is_deleted=false ";
			$sqlStr=$sqlStr."and ift.is_deleted=false ";

			$stmt =$this->pdo->prepare($sqlStr);
			$stmt->bindValue(":dr_tbl_id", $this->dr_tbl_id);
			$stmt->bindValue(":conf_tbl_id", $this->conf_tbl_id);
			$stmt->execute();

			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

			if ($stmt->rowCount()>0) 
			{

				foreach($rows as $item)
				{
					// 注文情報の明細情報登録
					$sqlStr="INSERT INTO order_meisai_tbl(order_tbl_id,item_tbl_id,is_deleted)VALUES(";
					$sqlStr=$sqlStr.":order_tbl_id,";//order_unique_string
					$sqlStr=$sqlStr.":item_id,";//item_id
					$sqlStr=$sqlStr."0";//is_deleted
					$sqlStr=$sqlStr.");";

					$stmt =$this->pdo->prepare($sqlStr);
					$stmt->bindValue(":item_id", $item['item_id']);
					$stmt->bindValue(":order_tbl_id", $order_tbl_id);
					$stmt->execute();
					$stmt=null;


					// カンファランス申し込み登録
					if($item['item_type']==1)
					{
						$sqlStr="INSERT INTO conf_link_tbl (conf_tbl_id, dr_tbl_id,order_tbl_id,is_paid,is_ok,is_deleted) VALUES (:conf_tbl_id, :dr_tbl_id,:order_tbl_id,1,1,0)";

						$stmt =$this->pdo->prepare($sqlStr);
						$stmt->bindValue(":conf_tbl_id", $this->conf_tbl_id);
						$stmt->bindValue(":dr_tbl_id", $this->dr_tbl_id);
						$stmt->bindValue(":order_tbl_id", $order_tbl_id);
						$stmt->execute();
						$stmt=null;

					}
				}
	//**************************************************************
	//	2014/08/01変更（はじめ）
	//**************************************************************
				//変更を確定する
   				$this->pdo->commit();
			}
			else
			{
				throw new Exception("注文番号「".$AccessID."」が不正です。");

			}

	//		//変更を確定する
	//		$this->pdo->commit();
	//**************************************************************
	//	2014/08/01変更（おわり）
	//**************************************************************

			//接続断処理
			$stmt=null;

		}
		catch(Exception $e)
		{
			//変更をロールバックする

			if(is_null($this->pdo)!=true)
			{
	   			$this->pdo->rollBack();
			}
			//接続断処理
			$stmt=null;

			throw $e;
		}

	    }

		// 明細取得
		function get() {

			$tempStr="";
			$tempArr;

		 	try
			{

			// MySQLサーバへ接続
   			$this->pdo = $this->open();



			$sqlStr="SELECT ift.fee as fee, it.type as type, it.name as name ,dt.email as email ";
			$sqlStr=$sqlStr."FROM conf_tbl ct, ";
			$sqlStr=$sqlStr." dr_tbl dt, ";
			$sqlStr=$sqlStr." item_tbl it, ";
			$sqlStr=$sqlStr." item_fee_tbl ift ";
			$sqlStr=$sqlStr."where  ";
			$sqlStr=$sqlStr."ift.item_tbl_id = it.id "; 
			$sqlStr=$sqlStr."and dt.job_kind = ift.job_kinds_tbl_id ";
			$sqlStr=$sqlStr."and ct.item_tbl_id = it.id ";
			$sqlStr=$sqlStr."and dt.id=:dr_tbl_id ";
			$sqlStr=$sqlStr."and ct.id=:conf_tbl_id ";
			$sqlStr=$sqlStr."and ct.is_deleted=false ";
			$sqlStr=$sqlStr."and dt.is_deleted=false ";
			$sqlStr=$sqlStr."and it.is_deleted=false ";
			$sqlStr=$sqlStr."and ift.is_deleted=false ";

			$stmt =$this->pdo->prepare($sqlStr);
			$stmt->bindValue(":dr_tbl_id", $this->dr_tbl_id);
			$stmt->bindValue(":conf_tbl_id", $this->conf_tbl_id);
			$stmt->execute();


			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$tempArr=$rows;

			//接続断処理
			$stmt=null;

		}
		catch(PDOException $e)
		{
			//変更をロールバックする

			if(is_null($this->pdo)!=true)
			{
	   			$this->pdo=null;
			}
			throw $e;
		}

		return $tempArr;

	    }

		// レコード有無
		function hasRecord($order_unique_string) {

			$result=false;

		 	try
			{

			// MySQLサーバへ接続
   			$this->pdo = $this->open();

			if($this->hasRecordStmt==null)
			{
				$sqlStr="SELECT order_unique_string ";
				$sqlStr=$sqlStr."FROM order_tbl ";
				$sqlStr=$sqlStr."where  ";
				$sqlStr=$sqlStr."order_unique_string=:order_unique_string ";

				$this->hasRecordStmt =$this->pdo->prepare($sqlStr);
			}

			$this->hasRecordStmt->bindValue(":order_unique_string", $order_unique_string);
			$this->hasRecordStmt->execute();


			$rows = $this->hasRecordStmt->fetchAll(PDO::FETCH_ASSOC);
			
			if ($this->hasRecordStmt->rowCount()>0) 
			{
				$result=true;
			}


		}
		catch(PDOException $e)
		{
			//変更をロールバックする

			if(is_null($this->pdo)!=true)
			{
	   			$this->pdo=null;
			}
			throw $e;
		}

		return $result;

	    }

		// 新規登録
		function getStatementInsert($AccessID) {
			$result="";

			$order_tbl_id=null;

			        if (is_null($this->dr_tbl_id)==true)
				{
					$result="";
					throw new Exception("login info is not found");
				}

				// 注文情報のヘッダー登録
				$sqlStr="INSERT INTO order_tbl(order_unique_string,dr_tbl_id,is_ok,is_deleted,GMO_AccessID)VALUES(";
				$sqlStr=$sqlStr."'".$this->order_unique_string."',";//order_unique_string
				$sqlStr=$sqlStr.$this->dr_tbl_id.",";
				$sqlStr=$sqlStr."1,";//is_ok
				$sqlStr=$sqlStr."0,";//is_deleted
				$sqlStr=$sqlStr."'".$AccessID."'";
				$sqlStr=$sqlStr.");";


$result=$sqlStr."<br>";
$result=$result."/*    注文情報のorder_id取得*/<br>";



				// 注文情報のorder_id取得
				$sqlStr="    SELECT id ";
				$sqlStr=$sqlStr."FROM order_tbl ";
				$sqlStr=$sqlStr."where  ";
				$sqlStr=$sqlStr."order_unique_string='".$this->order_unique_string."'";//order_unique_string
				$sqlStr=$sqlStr." and is_deleted=false;";


$result=$result.$sqlStr."<br>";
$result=$result."/*    注文情報の明細情報登録*/<br>";
				$sqlStr="    SELECT it.id as item_id ";
//				$sqlStr=$sqlStr."SELECT ift.fee as fee, it.type as item_type, it.name as name ,it.id as item_id,dt.email as email ";
				$sqlStr=$sqlStr."FROM conf_tbl ct, ";
				$sqlStr=$sqlStr." dr_tbl dt, ";
				$sqlStr=$sqlStr." item_tbl it, ";
				$sqlStr=$sqlStr." item_fee_tbl ift ";
				$sqlStr=$sqlStr." where  ";
				$sqlStr=$sqlStr." ift.item_tbl_id = it.id "; 
				$sqlStr=$sqlStr." and dt.job_kind = ift.job_kinds_tbl_id ";
				$sqlStr=$sqlStr." and ct.item_tbl_id = it.id ";
				$sqlStr=$sqlStr." and dt.id=".$this->dr_tbl_id;
				$sqlStr=$sqlStr." and ct.id=".$this->conf_tbl_id;
				$sqlStr=$sqlStr." and ct.is_deleted=false ";
				$sqlStr=$sqlStr." and dt.is_deleted=false ";
				$sqlStr=$sqlStr." and it.is_deleted=false ";
				$sqlStr=$sqlStr." and ift.is_deleted=false ;";


$result=$result.$sqlStr."<br>";
$result=$result."/*注文情報の明細情報登録*/<br>";

				// 注文情報の明細情報登録
				$sqlStr="INSERT INTO order_meisai_tbl(order_tbl_id,item_tbl_id,is_deleted)VALUES(";
				$sqlStr=$sqlStr."上記で入手した「order_id」を設定,";//order_tbl_id
				$sqlStr=$sqlStr."上記で入手した「item_id」を設定,";//item_id
				$sqlStr=$sqlStr."0";//is_deleted
				$sqlStr=$sqlStr.");";


$result=$result.$sqlStr."<br>";
$result=$result."/*カンファランス申込*/<br>";


				//カンファランス申込
				$sqlStr="INSERT INTO conf_link_tbl (conf_tbl_id, dr_tbl_id,order_tbl_id,is_paid,is_ok,is_deleted) VALUES (".$this->conf_tbl_id.",".$this->dr_tbl_id.",上記で入手した「order_id」を設定,1,1,0);";
$result=$result.$sqlStr."<br><br><br>";

				return $result;
	    }


	}






	setlocale(LC_ALL, 'ja_JP.UTF-8');

	session_start();

	if (!isset($_SESSION['auth_tool_admin'])||($_SESSION['auth_tool_admin'] != hash("sha512", $magic_code.'facc'))) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}


	$temp = fopen($_FILES['importFile']['tmp_name'], "r");
	while ($csv[] = fgetcsv($temp, "1024")) {}
	fclose($temp);

//	$data = mb_convert_encoding($csv, 'UTF-8', 'sjis');
 

	$isHeader=0;
	$insertCount=0;
	$alertCount=0;

	$pdo=null;

 	try
	{

		$orderDao=new order_dao(null,null,null);

		foreach($csv as $value)
		{

			//ファイルの妥当性確認
			if(count($value)>=20)
			{
				if($value[0]==$ShopID)
				{

					$order_unique_string=$value[1];
					$arr=explode("-",$order_unique_string);				
//					[0]=orderId
//					[1]=dr_tbl_id
//					[2]=item_id

					if(count($arr)==3)
					{

						// レコード存在有無問い合わせ
						if($orderDao->hasRecord($order_unique_string)==false)
						{
							// DBへの登録
							if($order_unique_string!=null)
							{
							}
	
						}
						else
						{
//							echo "【△△△要確認△△△△】：＜注文番号＞".$value[1]." ＜状態＞ ".$value[2]." ＜GMOID＞ ".$value[15]."<br>";
							$alertCount=$alertCount+1;
						}
					}
					else
					{
						throw new Exception("注文番号が既定のフォーマットではありません。".print_r($value)."<br>");
					}//if(count($arr==3))
				}
				else
				{
					throw new Exception("ショップIDが異なります。".print_r($value)."<br>");

				}//if($value[0]==$ShopID)
			}
			else
			{
				if($value!="")
				{
					throw new Exception("ファイルフォーマットが異なります。".print_r($value)."<br>");
				}
			}////ファイルの妥当性確認 if(count($value)==23)

		}

		//チェック確認済みのため実施
		foreach($csv as $value)
		{
	//**************************************************************
	//	2014/08/21変更（はじめ）
	//**************************************************************


					$order_unique_string=$value[1];
					$arr=explode("-",$order_unique_string);				
//					[0]=orderId
//					[1]=dr_tbl_id
//					[2]=item_id

					if(count($arr)==3)
					{
						//決済済みの情報の場合
						if($value[2]=="CAPTURE")
						{
							// レコード存在有無問い合わせ
							if($orderDao->hasRecord($order_unique_string)==false)
							{
								// DBへの登録
								if($order_unique_string!=null)
								{
									$orderDao->setParameter($order_unique_string,$arr[1],$arr[2]);
									$orderDao->insert($value[15]);
//									echo $orderDao->getStatementInsert($value[15]);
									$insertCount=$insertCount+1;

									echo "【追加】：＜注文番号＞".$value[1]." ＜状態＞ ".$value[2]." ＜GMOID＞ ".$value[15]."<br>";

									// メール送信
									$mail=new gmo_mail();
	
									$mail->sendByDrId($arr[1]);

								}
							}
						}


					}
	//**************************************************************
	//	2014/08/21変更（おわり）
	//**************************************************************


		}

		$orderDao->close();


		echo "追加対象数：".$insertCount."<br>";

	}
	 catch(Exception $e)
	{
		echo $e->getMessage()."<br>";
		echo "登録作業を中止いたします。"."<br>";
	}

	





?> 

bbb