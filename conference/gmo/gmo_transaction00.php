﻿<?php
	//gmo_transaction00.php

	//**************************************************************
	//	注文予定情報登録およびGMOサイトへのリクエスト送信
	//	
	//**************************************************************

	//設定ファイルの読み込み
	require_once("../../utilities/config.php");
	require_once("../../utilities/lib.php");
	require_once("order_dao.php");

	//追加設定ファイルの読み込み
	require_once("../../utilities/ext_conf.php");

	mb_language("Japanese");
	mb_internal_encoding("UTF-8");
	mb_http_output('UTF-8');	
	session_start();


	//初期化

	$DateTime=date('YmdHis'); //日時情報 yyyyMMddhhmmss書式 例 20130514142305 mycartより受け取るように変更

   	$Amount=0;
	$Tax=0;

/*
	//リロード対策
	if (!isset($_SESSION['ticket'])) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied----------</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}
*/


	//DB更新
//	unset($_SESSION['ticket']);

	if (!array_key_exists("dr_tbl_id", $_POST))
	{
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}

	if (!array_key_exists("conf_tbl_id", $_POST))
	{
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}

    	$order_unique_string=$DateTime."-".$_POST['dr_tbl_id']."-".$_POST['conf_tbl_id'];//オーダーID

	// dr_tbl_id、conf_tbl_id情報より合計金額取得

	$orderDao=new order_dao($order_unique_string,$_POST['dr_tbl_id'],$_POST['conf_tbl_id']);

	$rows=$orderDao->get();

	foreach($rows as $item)
	{
	    $Amount=$Amount+$item['fee'];
	    $email=$item['email'];
	}

	
	$Tax=0;//$_POST["Tax"];


 	try
	{


		//MD5を取得する。
		$ShopPassString=$ShopID.$order_unique_string.$Amount.$Tax.$ShopPass.$DateTime;
		$ShopPassString=md5($ShopPassString);


		//リクエスト送信
		$data=array(
			'ShopID'=>$ShopID,
			'OrderID'=>$order_unique_string,
			'RetURL'=>$RetURL,
			'CancelURL'=>$CancelURL,
			'UseCredit'=>$UseCredit,
			'JobCd'=>$JobCd,
			'Amount'=>$Amount,
			'Tax'=>$Tax,
			'DateTime'=>$DateTime,
			'Confirm'=>$Confirm,
			'ShopPassString'=>$ShopPassString,
			'ClientField1'=>$email
		);
//		$data = http_build_query($data, "", "&");
 
		$header = array(
        		"Content-Type: application/x-www-form-urlencoded",
	        	"Content-Length: ".strlen($data)
		    );
                     
		$options =array(
    			'http' =>array(
	            	'method' => 'POST',
        	    	'header' => implode("\r\n", $header),
            		'content' => $data
	        	)
    		);
 
//		$contents =file_get_contents($url, false, stream_context_create($options));

//		echo mb_convert_encoding($contents,'UTF-8','auto');

	} catch(Exception $e){
		if($e->getCode()==23000)
		{

			$_SESSION['ticket']=date('YmdHis');

			//OrderID重複時の処理
			echo "現在、システムが混雑しました。<br>";
			echo "もう一度、「決済画面に進む」をクリックお願いします。<br><br>";

			echo "<form method=\"post\" action=\"gmo_transaction00.php\" id=\"form_to_GMO\">";
			echo "<input type=\"hidden\" name=\"dr_tbl_id\" value=\"".$_POST["dr_tbl_id"]."\">";
			echo "<input type=\"hidden\" name=\"conf_tbl_id\" value=\"".$_POST["conf_tbl_id"]."\">";
			echo "<input type=\"submit\" value=\"決済画面に進む\">";
			echo "</form>";
		}
		else
		{
	    		die($e->getMessage());
		}

	}

?>

<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script  type="text/javascript">
	function post()
	{
                document.forms["postData"].action="<?=$url?>";

                document.forms["postData"].ShopID.value="<?=$ShopID?>";
                document.forms["postData"].OrderID.value="<?=$order_unique_string?>";
                document.forms["postData"].RetURL.value="<?=$RetURL?>";
                document.forms["postData"].CancelURL.value="<?=$CancelURL?>";
                document.forms["postData"].UseCredit.value="<?=$UseCredit?>";
                document.forms["postData"].JobCd.value="<?=$JobCd?>";
                document.forms["postData"].Amount.value="<?=$Amount?>";
                document.forms["postData"].Tax.value="<?=$Tax?>";
                document.forms["postData"].DateTime.value="<?=$DateTime?>";
                document.forms["postData"].Confirm.value="<?=$Confirm?>";
                document.forms["postData"].ShopPassString.value="<?=$ShopPassString?>";
                document.forms["postData"].ClientField1.value="<?=$email?>";


                document.forms["postData"].submit();
	}
</script>

</head>
<body onload="post()">
	<form name="postData">
            <input type="hidden" name="ShopID" id="ShopID">
            <input type="hidden" name="OrderID" id="OrderID">
            <input type="hidden" name="RetURL" id="RetURL">
            <input type="hidden" name="CancelURL" id="CancelURL">
            <input type="hidden" name="UseCredit" id="UseCredit">
            <input type="hidden" name="JobCd" id="JobCd">
            <input type="hidden" name="Amount" id="Amount">
            <input type="hidden" name="Tax" id="Tax">
            <input type="hidden" name="DateTime" id="DateTime">
            <input type="hidden" name="Confirm" id="Confirm">
            <input type="hidden" name="ShopPassString" id="ShopPassString">
            <input type="hidden" name="ClientField1" id="ClientField1">
	</form>
</body>
</html>