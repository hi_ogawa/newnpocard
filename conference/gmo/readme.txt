conference\GMOに以下のファイルを設置お願いいたします。


gmo_transaction00.php
gmo_transaction01.php
ext_conf.php
OrderDao.php

ファイル説明
gmo_transaction00.php
	説明：指定されたパラメータを受け取り、GMOサイトへ遷移する。
	input：POSTで「dr_tbl_id」「conf_tbl_id」
	output：GMOサイト接続用パラメータのほか、金額、注文番号、戻りURL等をPOSTで送信
gmo_transaction01.php
	説明：DB登録を実施。
	input：POSTで「$OrderID」「AccessID」
	output：外部ファイル等を呼び出し、添付ファイル作成およびメール送信で検討中
ext_conf.php
	説明：GMOに接続するためのパラメータ等を設定

OrderDao.php
	説明：注文情報等をDBに登録
	メソッド：open：DBのコネクション取得
	メソッド：insert：order_tbl、order_meisai_tbl、conf_link_tblのレコード登録
	メソッド：insert：order_tbl、order_meisai_tbl、conf_link_tblのレコード登録
	メソッド：insert：get：「dr_tbl_id」「conf_tbl_id」に基づくitem_fee_tblレコード等を取得
