﻿<?php
	//select_ordered_info.php
	//**************************************************************
	//	利用者の注文情報表示
	//	
	//**************************************************************

	require_once("../../../utilities/config.php");
	require_once("../../../utilities/lib.php");	

	mb_language("Japanese");
	mb_internal_encoding("UTF-8");
	mb_http_output('UTF-8');

	session_start();

	if (!isset($_SESSION['auth_tool_admin'])||($_SESSION['auth_tool_admin'] != hash("sha512", $magic_code.'facc'))) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}
/*
	if (!isset($_POST['index_key'])||($_POST['index_key'] != hash("sha512", $magic_code))) {
		$_SESSION = array();
		header('Location: ../../../auth/tool_login.php');
	}
*/
?>

<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<link rel="stylesheet" type="text/css" href="../../../css/index.css"/>
<style type="text/css">
.center table{
	margin-left:auto;
	margin-right:auto;
}
.OrderedInfoTableHeader{
	background-color:grey;
	color:yellow;
	font-size:16px;
	font-weight:bold;
}
.OrderedInfoTableRowOdd{
	background-color:blue;
	color:white;
	font-size:16px;
	font-weight:bold;
}
.OrderedInfoTableRowEven{
	background-color:grey;
	color:yellow;
	font-size:16px;
	font-weight:bold;
}
</style>
 <script src="../../../javascript/jquery-1.10.2.js"></script>
<title>NPO Registration</title>

	<script type="text/javascript">
		var submitted = false
		function blockForm(){
			if( submitted ){
				return false
			}
			submitted = true
			return true
		}

		$(document).ready(
			function(){
				hideColExtra();
			}
		);//$(document).ready

		function showColExtra()
		{
			$(".col1").show();//番号
			$(".col2").show();//日付
			$(".col3").show();//名前
			$(".col4").show();//メールアドレス
			$(".col5").show();//施設名
			$(".col6").show();//支払アイテム名
			$(".col7").show();//区分
			$(".col8").show();//金額
			$(".col9").show();//アルファベット性
			$(".col10").show();//アルファベット名
			$(".col11").show();//注文番号

		}//end of function showColExtra()

		function hideColExtra()
		{
//			$(".col1").hide();//番号
			$(".col2").hide();//日付
//			$(".col3").hide();//名前
//			$(".col4").hide();//メールアドレス
			$(".col5").hide();//施設名
			$(".col6").hide();//支払アイテム名
			$(".col7").hide();//区分
			$(".col8").hide();//金額
			$(".col9").hide();//アルファベット性
			$(".col10").hide();//アルファベット名
			$(".col11").hide();//注文番号

		}//end offunction hideColExtra()

		function nextAction4Col()
		{
			if($("#nextActionForCol").val()=="＋（詳細情報表示）")
			{
				showColExtra();
				$("#nextActionForCol").val("－（詳細情報を隠す）");
			}
			else
			{
				hideColExtra();
				$("#nextActionForCol").val("＋（詳細情報表示）");
			}

		}//end of function nextAction4Col()

	</script>
</head>

<body>
<div id="title">
NPOティー・アール・アイ国際ネットワーク登録システム
<div id="eng_title">Registration System of NPO TRI International Network</div>
</div>
<div class="center">
	<h3 class="index_table">支払済み情報は以下です。</h3>

    	<form id="form1" action="select_ordered_info.php" method="post" onsubmit="return blockForm()">
        	<input type="hidden" id="kubun" name="kubun" value="-1">
        	<input type="button" value="すべて表示" onclick="$('#kubun').val('-1');$('#form1').submit();">
        	<input type="button" value="Drのみ表示" onclick="$('#kubun').val('0');$('#form1').submit();">
        	<input type="button" value="コメディカルのみ表示" onclick="$('#kubun').val('1');$('#form1').submit();">
        	<input type="button" value="企業のみ表示" onclick="$('#kubun').val('2');$('#form1').submit();">
        	<input type="text" id="search_key" name="search_key" value="">
        	<input type="submit" value="検索">
	</form>
<?php
	if(isset($_POST['kubun'])!=true)
	{
		echo "検索対象職種区分：Dr、コメディカル、企業";
	}
	else
	{
		if($_POST['kubun']==-1)
		{
			echo "検索対象職種区分：Dr、コメディカル、企業";
		}
		if($_POST['kubun']==0)
		{
			echo "検索対象職種区分：Dr";
		}
		if($_POST['kubun']==1)
		{
			echo "検索対象職種区分：コメディカル";
		}
		if($_POST['kubun']==2)
		{
			echo "検索対象職種区分：企業";
		}
	}

	$pdo=null;
	$number=1;

 	try
	{
				global $db_host;
				global $db_name;
				global $db_user;
				global $db_password;

			// MySQLサーバへ接続
			$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
			$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

			$sqlStr="SELECT dt.dr_name as dr_name,dt.hp_name as hp_name, dt.email as email, ot.order_unique_string as order_unique_string, it.name as item_name, ift.fee as item_fee,dt.job_kind as job_kind,dt.sirname as sirname ,dt.firstname as firstname, ot.order_unique_string as order_unique_string " ;
			$sqlStr=$sqlStr." FROM ( " ;
			$sqlStr=$sqlStr." 	( " ;
			$sqlStr=$sqlStr." 		(order_tbl ot INNER JOIN dr_tbl dt ON ot.dr_tbl_id = dt.id) " ;
			$sqlStr=$sqlStr." 		 INNER JOIN order_meisai_tbl omt ON ot.id = omt.order_tbl_id " ;
			$sqlStr=$sqlStr." 	) " ;
			$sqlStr=$sqlStr." 	 INNER JOIN item_tbl it ON omt.item_tbl_id = it.id " ;
			$sqlStr=$sqlStr."      ) " ;
			$sqlStr=$sqlStr."      INNER JOIN item_fee_tbl ift ON (it.id = ift.item_tbl_id) AND (dt.job_kind = ift.job_kinds_tbl_id) " ;

			$sqlStr=$sqlStr." WHERE (ot.is_deleted=0 AND omt.is_deleted=0 AND ot.is_ok=1 AND dt.is_deleted=0 AND dt.is_active=1 AND dt.is_usable=1 AND it.is_deleted=0 AND ift.is_deleted=0) " ; 
			$sqlStr=$sqlStr." AND  (dt.dr_name like :search_key or dt.email like :search_key or ot.order_unique_string like :search_key or it.name like :search_key or ift.fee like :search_key  or dt.hp_name like :search_key) " ;

			if(isset($_POST['kubun']))
			{
				if($_POST['kubun']==0)
				{
					$sqlStr=$sqlStr." AND  dt.job_kind in (1)" ;
				}
				if($_POST['kubun']==1)
				{
					$sqlStr=$sqlStr." AND  dt.job_kind in (2,3,4,5,6,7,8,9)" ;
				}
				if($_POST['kubun']==2)
				{
					$sqlStr=$sqlStr." AND  dt.job_kind in (10)" ;
				}
			}


			$sqlStr=$sqlStr." ORDER BY dt.sirname,dt.firstname,ot.order_unique_string desc" ; 

			$stmt =$pdo->prepare($sqlStr);
			$search_key="";

			if (isset($_POST['search_key'])==true)
			{
				$search_key="%".$_POST['search_key']."%";
			}
			else
			{
				$search_key="%";
			}


			$stmt->bindValue(":search_key", $search_key);

			$stmt->execute();


			$data=array();

			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>
　　<br>検索件数：<?=$stmt->rowCount()?>件
    <input type="button" id="nextActionForCol" value="＋（詳細情報表示）" onclick="nextAction4Col()">
 
    <table border=1>
    <tr>
	<th class="col1 OrderedInfoTableHeader"><input type="button" value="－" onclick="$('.col1').hide()">番号</th>
	<th class="col2 OrderedInfoTableHeader"><input type="button" value="－" onclick="$('.col2').hide()">日付</th>
	<th class="col3 OrderedInfoTableHeader"><input type="button" value="－" onclick="$('.col3').hide()">名前</th>
	<th class="col4 OrderedInfoTableHeader"><input type="button" value="－" onclick="$('.col4').hide()">メールアドレス</th>
	<th class="col5 OrderedInfoTableHeader"><input type="button" value="－" onclick="$('.col5').hide()">施設名</th>
	<th class="col6 OrderedInfoTableHeader"><input type="button" value="－" onclick="$('.col6').hide()">支払アイテム名</th>
	<th class="col7 OrderedInfoTableHeader"><input type="button" value="－" onclick="$('.col7').hide()">区分</th>
	<th class="col8 OrderedInfoTableHeader"><input type="button" value="－" onclick="$('.col8').hide()">金額</th>
	<th class="col9 OrderedInfoTableHeader"><input type="button" value="－" onclick="$('.col9').hide()">アルファベット性</th>
	<th class="col10 OrderedInfoTableHeader"><input type="button" value="－" onclick="$('.col10').hide()">アルファベット名</th>
	<th class="col11 OrderedInfoTableHeader"><input type="button" value="－" onclick="$('.col11').hide()">注文番号</th>
    </tr>

<?php
			if ($stmt->rowCount()>0) 
			{
				$index=0;

				foreach($rows as $value)
				{
					if($value['order_unique_string']!=null)
					{
						$arr=explode("-",$value['order_unique_string']);
						$kessaiDate=$arr[0];
					}
					else
					{
						$kessaiDate="";
					}
?>
	<tr>           
<?
	if(($number % 2)==1)
	{
?>
	    	<td  class="col1 OrderedInfoTableRowOdd"><?= _Q($number) ?></td>
	    	<td  class="col2 OrderedInfoTableRowOdd"><?= _Q(date('Y年m月d日H時i分s秒',strtotime($kessaiDate))) ?></td>
    		<td  class="col3 OrderedInfoTableRowOdd"><?= _Q($value['dr_name']) ?></td>
    		<td  class="col4 OrderedInfoTableRowOdd"><?= _Q($value['email']) ?></td>
    		<td  class="col5 OrderedInfoTableRowOdd"><?= _Q($value['hp_name']) ?></td>
    		<td  class="col6 OrderedInfoTableRowOdd"><?= _Q($value['item_name']) ?></td>
		<td  class="col7 OrderedInfoTableRowOdd">
			<?php	if($value['job_kind']==1){ ?>Dr
			<?php	}else if($value['job_kind']==2){?>コメディカル
			<?php	}else if($value['job_kind']==3){?>コメディカル
			<?php	}else if($value['job_kind']==4){?>コメディカル
			<?php	}else if($value['job_kind']==5){?>コメディカル
			<?php	}else if($value['job_kind']==6){?>コメディカル
			<?php	}else if($value['job_kind']==7){?>コメディカル
			<?php	}else if($value['job_kind']==8){?>コメディカル
			<?php	}else if($value['job_kind']==9){?>コメディカル
			<?php	}else if($value['job_kind']==10){?>企業
			<?php	}else{?>その他
			<?php	}?>
		</td>
    		<td  class="col8 OrderedInfoTableRowOdd"><?= _Q($value['item_fee']) ?></td>
    		<td  class="col9 OrderedInfoTableRowOdd"><?= _Q($value['sirname']) ?></td>
    		<td  class="col10 OrderedInfoTableRowOdd"><?= _Q($value['firstname']) ?></td>
    		<td  class="col11 OrderedInfoTableRowOdd"><?= _Q($value['order_unique_string']) ?></td>
<?
	}
	else
	{
?>
	    	<td  class="col1 OrderedInfoTableRowEven"><?= _Q($number) ?></td>
	    	<td  class="col2 OrderedInfoTableRowEven"><?= _Q(date('Y年m月d日H時i分s秒',strtotime($kessaiDate))) ?></td>
    		<td  class="col3 OrderedInfoTableRowEven"><?= _Q($value['dr_name']) ?></td>
    		<td  class="col4 OrderedInfoTableRowEven"><?= _Q($value['email']) ?></td>
    		<td  class="col5 OrderedInfoTableRowEven"><?= _Q($value['hp_name']) ?></td>
    		<td  class="col6 OrderedInfoTableRowEven"><?= _Q($value['item_name']) ?></td>
		<td  class="col7 OrderedInfoTableRowEven">
			<?php	if($value['job_kind']==1){ ?>Dr
			<?php	}else if($value['job_kind']==2){?>コメディカル
			<?php	}else if($value['job_kind']==3){?>コメディカル
			<?php	}else if($value['job_kind']==4){?>コメディカル
			<?php	}else if($value['job_kind']==5){?>コメディカル
			<?php	}else if($value['job_kind']==6){?>コメディカル
			<?php	}else if($value['job_kind']==7){?>コメディカル
			<?php	}else if($value['job_kind']==8){?>コメディカル
			<?php	}else if($value['job_kind']==9){?>コメディカル
			<?php	}else if($value['job_kind']==10){?>企業
			<?php	}else{?>その他
			<?php	}?>
		</td>
    		<td  class="col8 OrderedInfoTableRowEven"><?= _Q($value['item_fee']) ?></td>
    		<td  class="col9 OrderedInfoTableRowEven"><?= _Q($value['sirname']) ?></td>
    		<td  class="col10 OrderedInfoTableRowEven"><?= _Q($value['firstname']) ?></td>
    		<td  class="col11 OrderedInfoTableRowEven"><?= _Q($value['order_unique_string']) ?></td>
<?
	}
?>
	</tr>           
<?
					$number=$number+1;

				}
			}
			//接続断処理
			$stmt=null;
			$pdo=null;



	}
	 catch(PDOException $e)
	{
    		die($e->getMessage());
	}
?> 
    </table>

</div>
</body>
</html>
	
?>
