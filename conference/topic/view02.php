<?php		// view02.php

	require_once('../../utilities/config.php');
	require_once('../../utilities/lib.php');	
	charSetUTF8();
	session_start();
	if (!isset($_SESSION['index_key'])||($_SESSION['index_key'] != hash("sha512", $magic_code))) {
		$_SESSION = array();
		header('Location: ../../index.php');
	}

	if (!auth_dr()) {
		$_SESSION = array();
		header('Location: ../../index.php');
	}

	
 	//入力エラーのチェック
	$error='';
	
	
 	try {
    // MySQLサーバへ接続
   	$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e){
    		die($e->getMessage());
	}

	if (($_POST['topic_title']=='')&&($_POST['role_kind']<3)) $error.='Topic Title is blank!<br />';
		else $_SESSION['topic_title'] = $_POST['topic_title'];
	if (!mb_check_encoding($_POST['topic_title'], 'UTF-8')) $error.='Unpermitted character set is used in Topic Title!<br />';
	if (strlen($_POST['topic_title'])>128) $error.='Too long Topic Title!<br />';
	if (($_POST['topic_abstract']=='')&&($_POST['role_kind']<3)) $error.='Topic Abstract is blank!<br />';
		else $_SESSION['topic_abstract'] = $_POST['topic_abstract'];
	if (!mb_check_encoding($_POST['topic_abstract'], 'UTF-8')) $error.='Unpermitted character set is used in Topic Abstract!<br />';
	if (strlen($_POST['topic_abstract'])>2000) $error.='Too long Topic Abstract!<br />';	

	//エラーのある場合
	if ($error != '') {
?>
<html><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xlmns="http://www.w3.org/1993/xhtml" xml:lang="ja" lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="../../mem_reg/member_registration.css">
 	<title>Your Abstract</title>
</head>
<body>
	<div align="center">
	<h1>Presentation Registration</h1><br/><br/>
	<p id="error"><?=$error ?></p><br />	
    <h3>Return and correct errors using [RETURN BUTTON] of Brwoser</h3>
	<h3>ブラウザの「戻る」ボタンを用いて戻り、間違いを訂正して下さい</h3>    
    <h3>You have to come back using "return" button of your browser, and correct the listed errors!</h3>
	</div>
</body>
</html>
<?php
		exit();
	} 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--
/*********************************************************
	2014/07/15変更（はじめ）
*********************************************************/
<script type="text/javascript" src="../../ajax/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../../ajax/jquery-corner.js"></script>
-->
<script type="text/javascript" src="../../javascript/jquery-1.10.2.js"></script>
<script type="text/javascript" src="../../javascript/jquery-corner.js"></script>
<!--
/*********************************************************
	2014/07/15変更（おわり）
*********************************************************/
-->

<script type="text/javascript" src="view02.js"></script>
<link rel="stylesheet" type="text/css" href="topic_css.css"/>
<title>Presentation Registration</title>
</head>
<body bgcolor="#AADDFF">
<div align="center">
<h1>Presentation Registration<br/><br/></h1>


<form action="view03.php" method="post" name="frm">
<!--
/*********************************************************
	2014/07/15変更（はじめ）
*********************************************************/
	<input type="hidden" name="dr_tbl_id" value="<?= $_POST['dr_tbl_id'] ?>" />
    <input type="hidden" name="conf_ename" value="<?= $_POST['conf_ename'] ?>" />
    <input type="hidden" name="conf_tbl_id" value="<?= $_POST['conf_tbl_id'] ?>" />
    <input type="hidden" name="dr_name" value="<?= $_POST['dr_name'] ?>" />
    <input type="hidden" name="dr_name_alpha" value="<?= $_POST['dr_name_alpha'] ?>" />
    <input type="hidden" name="role_kind" value="<?= $_POST['role_kind'] ?>" />
    <input type="hidden" name="topic_title" value="<?= $_POST['topic_title'] ?>" />
    <input type="hidden" name="topic_abstract" value="<?= $_POST['topic_abstract'] ?>" />
    <input type="hidden" name="role_tbl_id" value="<?= $_POST['role_tbl_id'] ?>" />
-->
    <input type="hidden" name="dr_tbl_id" value="<?= _Q($_POST['dr_tbl_id']) ?>" />
    <input type="hidden" name="conf_ename" value="<?= _Q($_POST['conf_ename']) ?>" />
    <input type="hidden" name="conf_tbl_id" value="<?= _Q($_POST['conf_tbl_id']) ?>" />
    <input type="hidden" name="dr_name" value="<?= _Q($_POST['dr_name']) ?>" />
    <input type="hidden" name="dr_name_alpha" value="<?= _Q($_POST['sirname']) ?>" />
    <input type="hidden" name="role_kind" value="<?= _Q($_POST['role_kind']) ?>" />
    <input type="hidden" name="topic_title" value="<?= _Q($_POST['topic_title']) ?>" />
    <input type="hidden" name="topic_abstract" value="<?= _Q($_POST['topic_abstract']) ?>" />
    <input type="hidden" name="role_tbl_id" value="<?= _Q($_POST['role_tbl_id']) ?>" />
<!--
/*********************************************************
	2014/07/15変更（おわり）
*********************************************************/
-->
    
<table>    
   	<tr><td class="title">Conference Name: </td><td class="title"><?= _Q($_POST['conf_ename']) ?></td></tr>
    <tr><td class="title">氏名 : </td><td class="title"><?= _Q($_POST['dr_name']) ?></td></tr>
<!--
/*********************************************************
	2014/07/15変更（はじめ）
*********************************************************/
    <tr><td class="title">English Name :  </td><td class="title"><?= _Q($_POST['dr_name_alpha']) ?></td></tr>     
-->
    <tr><td class="title">English Name :  </td><td class="title"><?= _Q($_POST['sirname']) ?></td></tr>     
<!--
/*********************************************************
	2014/07/15変更（おわり）
*********************************************************/
-->
	<tr><td class="title">Role : </td><td class="title"><?= _Q($role_kinds[$_POST['role_kind']]) ?></td></tr>
	<tr><td class="title">Topic Title : </td><td><textarea cols="60" rows="3" readonly><?= _Q($_POST['topic_title']) ?></textarea></td></tr>
<tr>
	<tr><td class="title">Topic Abstract : </td><td><textarea cols="60" rows="10" readonly><?= _Q($_POST['topic_abstract']) ?></textarea></td></tr>   
</table>
</form> 
<br /><br />
<div id="responce"><button id="confirm" class="confirm">SUBMIT (投稿する)</button>　　　　　　　　<button id="cancel">Cancel (戻る)</button></div>
<br /><br />


</div>
</body>
</html>
