<?php		// topic03.php

	require_once('../../utilities/config.php');
	require_once('../../utilities/lib.php');	
	charSetUTF8();
	session_start();
	if (!isset($_SESSION['index_key'])||($_SESSION['index_key'] != hash("sha512", $magic_code))) {
		$_SESSION = array();
		header('Location: ../../index.php');
	}

	if (!isset($_POST['dr_tbl_id'])||!is_numeric($_POST['dr_tbl_id'])||!isset($_POST['conf_tbl_id'])||!is_numeric($_POST['conf_tbl_id'])) {
		echo "<body bgcolor='red'>";
		echo "<h1 align='center'><font color='white'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	} else {
		$conf_tbl_id = $_POST['conf_tbl_id'];
		$dr_tbl_id = $_POST['dr_tbl_id'];
	}

	if (!auth_dr()) {
		$_SESSION = array();
		header('Location: ../../index.php');
	}

	
 	//入力エラーのチェック
	$error='';
	
// session変数に格納	
	$_SESSION['dr_tbl_id'] = $_POST['dr_tbl_id'];
	$_SESSION['conf_tbl_id'] = $_POST['conf_tbl_id'];
	
	//接続
 	try {
    // MySQLサーバへ接続
   	$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e){
    	die($e->getMessage());
	}

	$stmt = $pdo->prepare("SELECT * FROM `conf_tbl` WHERE `id` = :conf_tbl_id;");
	$stmt->bindValue(":conf_tbl_id", $conf_tbl_id);
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$_SESSION['conf_jname'] = $row['conf_jname'];
	$_SESSION['conf_ename'] = $row['conf_ename'];
	
	$stmt = $pdo->prepare("SELECT * FROM `dr_tbl` WHERE `id` = :dr_tbl_id;");
	$stmt->bindValue(":dr_tbl_id", $dr_tbl_id);
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$_SESSION['sirname'] = $row['sirname'];	
	$_SESSION['firstname'] = $row['firstname'];	
	$_SESSION['dr_name'] = $row['dr_name'];
	
	if (!isset($_POST['role_kind'])||!is_numeric($_POST['role_kind'])||($_POST['role_kind']<1)||($_POST['role_kind']>8)) $error.='Illegal role!<br />';
		else $_SESSION['role_kind'] = $_POST['role_kind'];
	if ((!isset($_POST['topic_title'])||($_POST['topic_title']==''))&&($_POST['role_kind']<3)) $error.='Topic Title is blank!<br />';
		else if (!isset($_POST['topic_title'])) $_SESSION['topic_title'] = "";
		else $_SESSION['topic_title'] = $_POST['topic_title'];
	if (!mb_check_encoding($_POST['topic_title'], 'UTF-8')) $error.='Unpermitted character set is used in Topic Title!<br />';
	if (strlen($_POST['topic_title'])>128) $error.='Too long Topic Title!<br />';
	if ((!isset($_POST['topic_abstract'])||($_POST['topic_abstract']==''))&&($_POST['role_kind']<3)) $error.='Topic Abstract is blank!<br />';
		else if (!isset($_POST['topic_abstract'])) $_SESSION['topic_abstract'] = "";
		else $_SESSION['topic_abstract'] = $_POST['topic_abstract'];
	if (!mb_check_encoding($_POST['topic_abstract'], 'UTF-8')) $error.='Unpermitted character set is used in Topic Abstract!<br />';
	if (strlen($_POST['topic_abstract'])>2000) $error.='Too long Topic Abstract!<br />';

	
 	try {
    // MySQLサーバへ接続
   	$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e){
    		die($e->getMessage());
	}
	// 同一者から同一演題名が提出されていないかチェック
	$stmt = $pdo->prepare("SELECT * FROM `role_tbl` WHERE `conf_tbl_id` = :conf_tbl_id AND `dr_tbl_id` = :dr_tbl_id AND `topic_title` = :topic_title;");
	$stmt->bindValue(":conf_tbl_id", $_POST['conf_tbl_id']);
	$stmt->bindValue(":dr_tbl_id", $_POST['dr_tbl_id']);
	$stmt->bindValue(":topic_title", $_POST['topic_title']);
	$stmt->execute();
	if ($stmt->rowCount()>0) $error.='You have already submitted that topic!';
	//
	


	//エラーのある場合
	if ($error != '') {
?>
<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<script src="../../javascript/jquery-1.10.2.js"></script>
<script src="../../javascript/jquery-corner.js"></script>
<script src="topic.js"></script>
    <link rel="stylesheet" type="text/css" href="../../mem_reg/member_registration.css">
 	<title>Your Presentation</title>
</head>
<body>
	<div align="center">
	<h1>Presentation Registration</h1><br/><br/>
	<p id="error"><?=$error ?></p><br />	
	<h3>ブラウザの「戻る」ボタンを用いて戻り、間違いを訂正して下さい</h3>
    <h3>You have to come back using "return" button of your browser, and correct the listed errors!</h3>
	</div>
</body>
</html>
<?php
		exit();
	} 
?>

<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<script src="../../javascript/jquery-1.10.2.js"></script>
<script src="../../javascript/jquery-corner.js"></script>
<script src="topic1.js"></script>
<link rel="stylesheet" type="text/css" href="topic_css.css"/>
<title>Presentation Registration</title>
</head>

<body>

<div id="main">
<h1>Contributing Your Presentation</h1>
<h2>This is your presentation during <?= $_SESSION['conf_ename'] ?>.<br />
これは <?= $_SESSION['conf_jname'] ?>におけるあなたの演題登録です</h2>

<!--
/*********************************************************
	2014/07/11変更（はじめ）
*********************************************************/
<form action="topic03.php" method="post" name="frm">
-->
<form action="topic04.php" method="post" name="frm">
<!--
/*********************************************************
	2014/07/11変更（はじめ）
*********************************************************/
-->

	<input type="hidden" name="dr_tbl_id" value="<?= _Q($dr_tbl_id) ?>">
    <input type="hidden" name="conf_tbl_id" value="<?= _Q($conf_tbl_id) ?>">
    <input type="hidden" name="role_kind" value="<?= _Q($_SESSION['role_kind']) ?>">
    <input type="hidden" name="topic_title" value="<?= _Q($_SESSION['topic_title']) ?>">
    <input type="hidden" name="topic_abstract" value="<?= _Q($_SESSION['topic_abstract']) ?>">
    <table>	
    <tr><td class="title">Conference Name: </td><td><?= _Q($_SESSION['conf_ename']) ?></td></tr>
    <tr><td class="title">氏名 : </td><td><?= _Q($_SESSION['dr_name']) ?></td></tr>
    <tr><td class="title">Your Name</td><td><?= _Q($_SESSION['sirname']).' '._Q($_SESSION['firstname']);?></td></tr>     
	<tr><td class="title">Your Role</td><td><?= $role_kinds[$_POST['role_kind']] ?></td></tr>
	<tr><td class="title">Presentation Title :</td><td><textarea readonly cols="60" rows="3"><?= _Q($_SESSION['topic_title']) ?></textarea></td></tr>
	<tr><td class="title">Presentation Abstract :</td><td><textarea readonly cols="60" rows="10"><?= _Q($_SESSION['topic_abstract']) ?></textarea></td></tr>
    </table> 
    </form>
<br /><br />
<div id="responce"><button class="confirm">SUBMIT as Above<br>(このまま投稿する)</button>　　　　　　　　<button id="cancel">BACK to Modify<br>(修正のため戻る)</button></div>
<br /><br />
</div>
</body>

</html>
