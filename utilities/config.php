<?php

// config.php

if ($_SERVER['SERVER_NAME'] === 'www.kamakuralive.net') {
	$db_host = '127.0.0.1';
	$db_user = 'kamuvnpqr';
	$db_password = 'cdEFWXWXQR';
	$site_url = 'https://www.kamakuralive.net/members/';
} elseif ($_SERVER['SERVER_NAME'] === 'localhost') {
	$db_host = 'localhost';
	$db_user = 'root';
	$db_password = 'faccfscai';
	$site_url = 'localhost/member_git/';
} else {
	die('illegal connection<br />');
}


$db_name = 'kamakuralive_members';
$registration_deadline = 1;		// 参加登録は1日前まで
$abstract_deadline = 0;	// 抄録受け付けは0日前まで


// site
$site_name = 'NPO TRI International Network';
$title = 'KAMAKURA LIVE DEMONSTRATION';
$support_mail = 'fightingradialist@tri-international.org';

// magic code
$admin_id = 'Dr_Radialist';
$magic_code = 'NAUSICA in KAMAKURA';

$hp_admin_id = 'KAMAKURA';
$hp_admin_pwd = 'Research Center';

$job_kinds = array(
	  "0"=>'Not Selected (未選択)',
	  "1"=>'Doctor',
      "2"=>'Nurse/Sister',
	  "3"=>'CRC (Crinical Research Coordinator)',
      "4"=>'Clinical Engineer',
      "5"=>'Radiology Engineer',
      "6"=>'CRA (Clinical Research Associate)',
      "7"=>'CRO (Contract Research Organization)',
      "8"=>'SMO (Site Management Organization)',
      "9"=>'Officials/Authorities',
      "10"=>'Company Persons and Others'
	  );
	  
$hints = array(
	"0"=>'Not Selected (未選択)',
	'1'=>"What is your mother's original name? (母の旧姓は?)",
	'2'=>'What is your favorite color? (一番好きな色は?)',
	'3'=>'What is your fun team? (どのチームのファン?)',
	'4'=>'What color do you hate? (大嫌いな色は?)',
	'5'=>'Who was your first boy/girl friend? (最初の彼氏/彼女の名は?)'
	);

$role_kinds = array(
	"1"=>'Study Presenter',
	"2"=>'Case Presenter',
	"3"=>'Chairman',
	"4"=>'Discussant',	
	"5"=>'Operator',
	"6"=>'IVUS Commentator',
	"7"=>'OCT Commentator'
	);

$country_codes = array(
	"0"=>'Not Selected (未選択)',
	"1"=>'JAPAN (日本)',
	"2"=>'South Korea (韓国)',
	"3"=>'China (中国)',
	"4"=>'Taiwan (台湾)',
	"5"=>'Hong Kong (香港)',
	"6"=>'Macao (マカオ)',
	"7"=>'Singapore (シンガポール)',
	"8"=>'Thailand (タイ)',
	"9"=>'Vietnam (ベトナム)',
	"10"=>'Malaysia (マレーシア)',
	"11"=>'India (インド)',
	"12"=>'Indonesia (インドネシア)',
	"13"=>'Bangladesh (バングラデシュ)',
	"14"=>'Phillipine (フィリピン)',
	"15"=>'Pakistan (パキスタン)',
	
	"30"=>'USA (米国)',
	"31"=>'Canada (カナダ)',
	"32"=>'Mexicao (メキシコ)',
	"33"=>'Costa Rica (コスタリカ)',
	"34"=>'Panama (パナマ)',
	"35"=>'Cuba (キューバ)',
	"36"=>'Colombia (コロンビア)',
	"37"=>'Brazil (ブラジル)',
	"38"=>'Alzentina (アルゼンチン)',
	"39"=>'Chilli (チリ)',
	"40"=>'Peru (ペルー)',
	
	"50"=>'France (フランス)',
	"51"=>'UK (大英帝国)',
	"52"=>'Germany (ドイツ)',	
	"53"=>'Russia (ロシア)',
	"54"=>'Spain (スペイン)',
	"99"=>'Others (これら以外)'
	);
	
$role_kinds = array(
	"1"=>'Study Presenter (研究発表者)',
	"2"=>'Case Presenter (症例報告者)',
	"3"=>'Chairman (座長)',
	"4"=>'Discussant (討論者)',	
	"5"=>'Operator (術者)',
	"6"=>'IVUS Commentator (IVUSコメンテーター)',
	"7"=>'OCT Commentator (OCTコメンテーター)'
	);
	
// paging
$minumum_no_of_pts_in_page = 3;

?>
