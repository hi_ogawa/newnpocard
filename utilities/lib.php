<?php	// lib.php
	/////////////////////////////////////////////////////////////////////////////////
	/* 保持せねばならない$_SESSION変数一覧
	/* $_SESSION['dr_tbl_id']
	/* $_SESSION['hp_tbl_id']
	/* $_SESSION['email']
	/* $_SESSION['auth_dr_code']
	/* $_SESSION'index_key']
	/* $_SESSION['last_time']についてはauth_dr(), auth_dr_long()の中で自動的に設定されるがセッションクリアの段階では必要
	/* $_SESSION['sirname'], $_SESSION['firstname'], $_SESSION['hp_name']に関しては必須ではないがサービスとして維持される
	////////////////////////////////////////////////////////////////////////////////*/
	
	function auth_hp() {
//　これは dummy functionであり、常に trueを戻す
		return true;
	}
	
	
	function auth_dr() {	
	//	return true;		// debug用	
		global $magic_code;
		//タイムアウトしていればログアウト処理
   	if (!isset( $_SESSION["last_time"])||(time() - $_SESSION["last_time"] > 600 )){
			 session_unset();   //$_SESSION = array();でも可？
    		//クッキーからセッションID削除
    		if (isset($_COOKIE["PHPSESSID"])) {
        		setcookie("PHPSESSID", '', time() - 42000, '/');
    		}
    		//セッション削除
    		session_destroy();
			if ($_SERVER['SERVER_NAME'] == 'https://www.kamakuralive.net') {
				header('Location: https://www.kamakuralive.net/members/');
				exit();
			} else {
				header('Location: http://localhost/member_git/');
				exit();
			}
    	} else {
			$_SESSION['last_time'] = time();	// session timeoutのための変数を再設定する
		} 
//		if ($_SERVER['SERVER_NAME'] === 'localhost') return TRUE;		// debug用	
		if (!isset($_SESSION['email'])||!isset($_SESSION['auth_dr_code'])) return FALSE;
		if (hash("sha512", $magic_code.$_SESSION['email']) == $_SESSION['auth_dr_code']) {
			return TRUE;
		} else {
			$_SESSION = array();			
			setcookie(session_name(), '', time()-42000, '/');
			session_destroy();
			return FALSE;
		}
	}
	
	function auth_dr_long() {	// 2時間後にタイムアウトする
		global $magic_code;
		//タイムアウトしていればログアウト処理
    	if (!isset( $_SESSION["last_time"])||(time() - $_SESSION["last_time"] > 60*60*2 )){
			 session_unset();   //$_SESSION = array();でも可？
    		//クッキーからセッションID削除
    		if (isset($_COOKIE["PHPSESSID"])) {
        		setcookie("PHPSESSID", '', time() - 42000, '/');
    		}
    		//セッション削除
    		session_destroy();
			if ($_SERVER['SERVER_NAME'] == 'http://www.kamakuralive.net') {
				header('Location: https://www.kamakuralive.net/members/');
				exit();
			} else {
				header('Location: http://localhost/member_git/');
				exit();
			}
    	} else {
			$_SESSION['last_time'] = time();	// // session timeoutのための変数を再設定する
		}
//		if ($_SERVER['SERVER_NAME'] === 'localhost') return TRUE;		// debug用	
		if (!isset($_SESSION['email'])||!isset($_SESSION['auth_dr_code'])) return FALSE;
		if (hash("sha512", $magic_code.$_SESSION['email']) == $_SESSION['auth_dr_code']) {
			return TRUE;
		} else {
			$_SESSION = array();			
			setcookie(session_name(), '', time()-42000, '/');
			session_destroy();
			return FALSE;
		}
	}
	
	function auth_admin() {
		global $magic_code;
		global $admin_id;
		if (!isset($_SESSION['auth_admin_code'])&&($_SERVER['SERVER_NAME'] == 'http://www.tri-international.org')) return FALSE;
		if (($_SERVER['SERVER_NAME'] == 'http://www.tri-international.org')||(hash("sha512", $admin_id.$magic_code) != $_SESSION['auth_admin_code'])) {
				return FALSE;
		} else {
			return TRUE;
		}
	}
	
	function auth_admin_general() {
		global $magic_code;		
		if ($_SERVER['SERVER_NAME'] == 'localhost') return TRUE;
		if (!isset($_SESSION['auth_admin_general'])&&($_SERVER['SERVER_NAME'] == 'http://www.tri-international.org')) return FALSE;
		if (($_SERVER['SERVER_NAME'] == 'http://www.tri-international.org')||(hash("sha512", $magic_code.'facc') != $_SESSION['auth_admin_genral'])) {
				return FALSE;
		} else {
			return TRUE;
		}
	}
	
	function check_auth_admin_general() {
		if (!auth_admin_general()) {
			echo "<body bgcolor='black'>";
			echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
			echo "</body>";
			session_destroy();
		exit();
		}	
	}
	
	function auth_index() {		// ちゃんとトップページから入ってきたか
								// さらにuser_agentをチェックしてセッション・ハイジャックを防ぐ
		return true;		// debug用
		global $magic_code;
		if (!isset($_SESSION['user_agent'])) $_SESSION['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
		if ($_SERVER['HTTP_USER_AGENT'] != $_SESSION['user_agent']) return false;
		if ($_SERVER['SERVER_NAME'] == 'localhost') return TRUE;
		if (!isset($_SESSION['index_key'])||($_SESSION['index_key'] != hash("sha512", $magic_code))) {
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	function check_auth_index() {
		if (!auth_index()) {
			echo "<body bgcolor='black'>";
			echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
			echo "</body>";
			session_destroy();
			exit();
		}
	}
	
	function get_hp_code($hp_login_name) {
		return substr(hash("sha512", $hp_login_name), 0, 6);
	}
	
    // 拡張子でMIME-typeを判定してみる例 
     
    // 必要に応じて、自分が利用したいMIME-typeを追加してください 
    $aContentTypes = array( 
        'txt'=>'text/plain', 
        'htm'=>'text/html', 
        'html'=>'text/html', 
        'jpg'=>'image/jpeg',
        'jpeg'=>'image/jpeg',
        'gif'=>'image/gif',
        'png'=>'image/png',
        'bmp'=>'image/x-bmp',
        'ai'=>'application/postscript',
        'psd'=>'image/x-photoshop',
        'eps'=>'application/postscript',
        'pdf'=>'application/pdf',
        'swf'=>'application/x-shockwave-flash',
        'lzh'=>'application/x-lha-compressed',
        'zip'=>'application/x-zip-compressed',
        'sit'=>'application/x-stuffit',
		'doc'=>'application/msword',
		'docx'=>'application/msword',
		'xlsx'=>'application/vnd.ms-excel',
		'xls'=>'application/msexcel',
		'ppt'=>'application/mspowerpoint'
    ); 

    // 拡張子からMimeTypeを設定 
    function getMimeType($filename) { 
        global $aContentTypes; 
        $sContentType = 'application/octet-stream'; 
         
        if (($pos = strrpos($filename, ".")) !== false) { 
            // 拡張子がある場合 
            $ext = strtolower(substr($filename, $pos+1)); 
            if (strlen($ext)) { 
                return $aContentTypes[$ext]?$aContentTypes[$ext]:$sContentType; 
            } 
        } 
        return $sContentType; 
    };
	
	// 文字コードのセット
	function charSetUTF8() {
		mb_language("Japanese");
		mb_internal_encoding("UTF-8");
		mb_http_output('UTF-8');
	};
	
	// HTML出力時のQuoting関数
	function _Q($argv) {
		return htmlentities($argv, ENT_QUOTES, 'UTF-8');
	}
	
?>