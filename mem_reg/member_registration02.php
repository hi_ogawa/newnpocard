<?php		// member_registration02.php

	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');	
	charSetUTF8();
	session_start();
	$today_year = date("Y");
	$_SESSION['auth_mem_reg'] = hash("sha512", $magic_code.$_POST['email']);
/*	
	if (!isset($_SESSION['mem_reg'])||($_SESSION['mem_reg'] != hash("sha512", $today_year))) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}
*/
 	//入力エラーのチェック
	$error='';
	if (!isset($_POST['dr_name'])||($_POST['dr_name']=='')) $error.='Your Name is absent (あなたの氏名がありません)<br />';
		else $_SESSION['dr_name'] = $_POST['dr_name'];
	if (!mb_check_encoding($_POST['dr_name'], 'UTF-8')) $error.='Your Name contains illegal characters (氏名が不正な文字列です)<br />';
	if (strlen($_POST['dr_name'])>64) $error.='Your Name is too long (氏名が長すぎます)<br />';
	if (!isset($_POST['sirname'])||($_POST['sirname']=='')) $error.='Your Sirname is absent (姓がありません)<br />';
		else $_SESSION['sirname'] = $_POST['sirname'];
	if (!mb_check_encoding($_POST['sirname'], 'UTF-8')) $error.='Your Sirname contains illegal characters (姓が不正な文字列です)<br />';
	if (!preg_match("/^[a-zA-Z]+$/",$_POST['sirname'])) $error.='Your Sirname has to be alphabeticals (姓に半角アルファベット以外の文字が使われています)<br />';
	if (strlen($_POST['sirname'])>16) $error.='Your Sirname is too long (姓が長すぎます)<br />';
	
	if (!isset($_POST['firstname'])||($_POST['firstname']=='')) $error.='Your Firstname is absent (名がありません)<br />';
		else $_SESSION['firstname'] = $_POST['firstname'];
	if (!mb_check_encoding($_POST['firstname'], 'UTF-8')) $error.='Your Firstname contains illegal characters (名が不正な文字列です)<br />';
	if (!preg_match("/^[a-zA-Z]+$/",$_POST['firstname'])) $error.='Your Firstname has to be alphabeticals (名に半角アルファベット以外の文字が使われています)<br />';
	if (strlen($_POST['firstname'])>16) $error.='Your Firstname is too long (名が長すぎます)<br />';	
	$dr_name_alpha = $_POST['sirname']." ".$_POST['firstname'];
			
	if (!isset($_POST['is_male'])||!is_numeric($_POST['is_male'])||($_POST['is_male']<1)||($_POST['is_male']>2)) $error.='Illegal Sex (不正な性別です)<br />';	
		else $_SESSION['is_male'] = $_POST['is_male'];
		
	if (!isset($_POST['birth_year'])||!is_numeric($_POST['birth_year'])||($_POST['birth_year']<1945)||($_POST['birth_year']>($today_year-20))) $error.='Illegal Birth Year (不正な生まれた年です)<br />';
		else $_SESSION['birth_year'] = $_POST['birth_year'];
		
	if (!isset($_POST['email'])||($_POST['email'] == '')) $error.='Your Email is absent (電子メール・アドレスがありません)<br />';
		else $_SESSION['email'] = $_POST['email'];
	if (strlen($_POST['email'])>64) $error.='Your Email is too long (電子メール・アドレスが長すぎます)<br />';
	if (!mb_check_encoding($_POST['email'], 'UTF-8')) $error.='Your Email contains illegal characters (電子メール・アドレスに不正な文字列が混入しています)<br />';
	$return = preg_match('/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/', $_POST['email']);
	if ($return == 0) $error .= 'Your Email is wrong (電子メール・アドレスが不正です)<br />';

	if (!isset($_POST['email2'])||($_POST['email2'] == '')) $error.='Confirm Email is absent (確認電子メール・アドレスがありません)<br />';
		else $_SESSION['email2'] = $_POST['email2'];
	if ($_POST['email'] != $_POST['email2']) $error .= 'Your Emails do not match (電子メールが一致しません)<br />';

	if (!isset($_POST['dr_pwd'])||($_POST['dr_pwd'] == '')) $error.='Your Password is absent (パスワードがありません)<br />';
	if (strlen($_POST['dr_pwd'])>32) $error.='Your Password is too long (パスワードが長すぎます)<br />';
	$return = preg_match('/[a-zA-Z0-9]+/', $_POST['dr_pwd']);
	if ($return == 0) $error .= 'Your Password contains illegal characters (不正な文字がパスワードに使用されています)<br />';
	if (!isset($_POST['dr_pwd2'])||($_POST['dr_pwd2'] == '')) $error.='Your Confirm Password is absent (確認パスワードがありません)<br />';	
	if ($_POST['dr_pwd'] != $_POST['dr_pwd2']) $error .= 'Your Passwords do not match (パスワードが一致しません)<br />';
	
	if (!isset($_POST['hp_name'])||($_POST['hp_name']=='')) $error.='Your Affiliation is absent (施設名がありません)<br />';	
		else $_SESSION['hp_name'] = $_POST['hp_name'];
	if (!mb_check_encoding($_POST['hp_name'], 'UTF-8')) $error.='Your Affiliation contains illegal characters (施設名が不正な文字列です)<br />';
	if (strlen($_POST['hp_name'])>128) $error.='Your Affiliation is too long (施設名が長すぎます)<br />';	
	
	if (!isset($_POST['hp_address'])||($_POST['hp_address']=='')) $error.='Address of Affiliation is absent (施設住所がありません)<br />';	
		else $_SESSION['hp_address'] = $_POST['hp_address'];
	if (!mb_check_encoding($_POST['hp_address'], 'UTF-8')) $error.='Affiliation address contains illegal characters (施設住所が不正な文字列です)<br />';
	if (strlen($_POST['hp_address'])>256) $error.='Affiliation address is too long (施設住所が長すぎます)<br />';	
	
	if (!isset($_POST['country_code'])||!is_numeric($_POST['country_code'])||($_POST['country_code']<1)||($_POST['country_code']>99)) $error.='Illegal country was selected (不正な国名です)<br />';	
		else $_SESSION['country_code'] = $_POST['country_code'];	
	
	if (!isset($_POST['job_kind'])||!is_numeric($_POST['job_kind'])||($_POST['job_kind']<1)||($_POST['job_kind']>10)) $error.='Your Specialty is illegal (不正な職種です)<br />';
		else $_SESSION['job_kind'] = $_POST['job_kind'];
		
	if (!isset($_POST['clue'])||!is_numeric($_POST['clue'])||($_POST['clue']<1)||($_POST['clue']>5)) $error.='Your Hint is illegal (不正なヒント選択です)<br />';
		else $_SESSION['clue'] = $_POST['clue'];
		
	if (!isset($_POST['hint'])||($_POST['hint']=='')) $error.='Hint Answer is absent (ヒント回答入力がありません)<br />';
		else $_SESSION['hint'] = $_POST['hint'];
	if (!mb_check_encoding($_POST['hint'], 'UTF-8')) $error.='Hint Answer contains illegal characters (ヒント回答に不正が不正な文字列です)<br />';
	if (strlen($_POST['hint'])>30) $error.='Hint Answer is too long (ヒント回答が長すぎます)<br />';

//接続
 	try {
    // MySQLサーバへ接続
   	$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e){
    	die($e->getMessage());
	}

	$stmt = $pdo->prepare("SELECT * FROM `dr_tbl` WHERE `email` = :email");
	$stmt->bindValue(":email", $_POST['email']);
	$stmt->execute();
	if ($stmt->rowCount() > 0) {
		$error.='That email address is already used (入力された電子メール・アドレスは既に登録されています)<br/>';
		$_SESSION['mem_reg'] = FALSE;
	} 

	//エラーのある場合
	if ($error != '') {

?>

<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<head>
	<script src="../javascript/jquery-1.10.2.js"></script>
    <link rel="stylesheet" type="text/css" href="member_registration.css">
 	<title>TRI Network</title>
</head>
<body>
	<div align="center">
	<h1>Registration of Yourself in KAMAKURA Live Demonstration</h1><br/><br/>
	<p id="error"><?=$error ?></p><br />	
	<h3>You have to return to the previous page to correct errors by pushing [RETURN] button of your browser.</h3>
	<h3>ブラウザの「戻る」ボタンを用いて戻り、間違いを訂正して下さい</h3>
	</div>
</body>
</html>
<?php
 	exit();
 }
?>

<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="member_registration.css"/>
	<script src="../javascript/jquery-1.10.2.js"></script>
    <script src="../javascript/jquery-corner.js"></script>
    <script src="dr_registration.js"></script>
<title>TRI Network</title>
</head>
<body bgcolor="#AADDFF">
<div align="center">
<h1>Registration of Yourself in KAMAKURA Live Demonstration</h1>

<table>
<form action="member_registration03.php" method="post">

	<input type="hidden" name="email" value="<?= _Q($_POST['email']) ?>" />
	<input type="hidden" name="dr_name" value="<?= _Q($_POST['dr_name']) ?>" />
    <input type="hidden" name="sirname" value="<?=_Q($_POST['sirname']) ?>" />
    <input type="hidden" name="firstname" value="<?=_Q($_POST['firstname']) ?>" />
    <input type="hidden" name="dr_pwd" value="<?= _Q($_POST['dr_pwd']) ?>" />
    <input type="hidden" name="hp_name" value="<?= _Q($_POST['hp_name']) ?>" />
    <input type="hidden" name="hp_address" value="<?= _Q($_POST['hp_address']) ?>" />
    <input type="hidden" name="country_code" value="<?= _Q($_POST['country_code']) ?>" />
    <input type="hidden" name="is_male" value="<?= _Q($_POST['is_male']) ?>" />
    <input type="hidden" name="birth_year" value="<?= _Q($_POST['birth_year']) ?>" />
    <input type="hidden" name="job_kind" value="<?= _Q($_POST['job_kind']) ?>" />
    <input type="hidden" name="clue" value="<?= _Q($_POST['clue']) ?>"  />
    <input type="hidden" name="hint" value="<?= _Q($_POST['hint']) ?>" />

    <tr>
<!--
/*********************************************************
	2014/07/16変更（はじめ）
*********************************************************/
-->
<!--
    <td>Your Fullname : </td><td class="check"><?= $_POST['dr_name'] ?></td></tr> 
-->
    <td>Your Fullname : </td><td class="check"><?= _Q($_POST['dr_name']); ?></td></tr> 
<!--
/*********************************************************
	2014/07/16変更（おわり）
*********************************************************/
-->
    <tr>
    <td>Your Sirname (あなたの姓) : </td><td  class="check"><?= _Q($_POST['sirname']);?></td></tr> 
    <tr>
    <td>Your Firstname (あなたの名) : </td><td class="check"><?= _Q($_POST['firstname']);	?></td></tr> 
 	<tr>
 	  <td>Your Sex (性別) : </td><td>
<!--
/*********************************************************
	2014/07/02変更（はじめ）
*********************************************************/
-->
<!--
  	<input type="radio" name="is_male" readonly <?php  if ($_POST['is_male'] == 1) echo "checked";	 ?>><span id="male">Male (男性)</span>
    <input type="radio" name="is_male" readonly  <?php if ($_POST['is_male'] == 2) echo "checked"; ?>><span id="female">Female (女性)</span></td></tr>
-->
  	<input type="radio" name="is_male" value="1" readonly <?php  if ($_POST['is_male'] == 1) echo "checked";	 ?>><span id="male">Male (男性)</span>
    <input type="radio" name="is_male" value="2" readonly  <?php if ($_POST['is_male'] == 2) echo "checked"; ?>><span id="female">Female (女性)</span></td></tr>
<!--
/*********************************************************
	2014/07/02変更（おわり）
*********************************************************/
-->


	<tr>
	  <td>Your Birth Year (西暦生まれ年) : </td><td>
    <select id="birth_year" name="birth_year" size="1" disabled><option selected><?= $_POST['birth_year'] ?></option></select></td></tr>

    </td></tr>
	<tr><td>Your Affiliation (ご所属) : <br /></td><td class="check"><?= _Q($_POST['hp_name']) ?></td></tr>
    
    	<tr><td>Address of Affiliation (所属先住所) : <br /></td><td class="check"><?= _Q($_POST['hp_address']) ?></td></tr>
    
   	<tr>
    <td>Your Country (国籍) :  </td>
    <td><select id="country_code" name="country_code" disabled><option selected><?= $country_codes[$_POST['country_code']] ?></option></select></td></tr>

    </td></tr>
    
	<tr><td>Your Specialty (ご専門) :  </td>
    <td><select id="job_kind" name="job_kind" disabled><option selected><?= $job_kinds[$_POST['job_kind']] ?></option></select></td></tr>

    </td></tr>
    
	<tr><td>Your E-mail <small><font color="red"> (= login ID)</font></small> : </td><td class="check"><?= _Q($_POST['email']) ?></td></tr> 
   
	<tr><td><p>Hint when forgetting password (パスワード紛失時ヒント) :<br />
    </p></td>
			
    <td><select id="clue" name="clue" disabled><option selected><?= $hints[$_POST['clue']] ?></option></select></td></tr>

        
   <tr><td>Your Answer for Hint (その問に対するあなたの回答) : </td><td class="check"><?= _Q($_POST['hint']) ?></tr>    
	<tr></tr>
<tr><td colspan="2" style="text-align:center"><input type="submit" value="Register as Above (上記情報で登録します)" id="submit_btn"></td></tr>

</form>
</table>
</div>
</body>
</html>
