﻿<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<head>
	<script src="../javascript/jquery-1.10.2.js"></script>
    <link rel="stylesheet" type="text/css" href="member_reg_mail.css">
	<title>ID Registration [3/4]</title>
</head>
<body>
<h1><br/>Your ID Registration<br/></h1>
<div align="center">
<table width="80%">
<tr><td class="r">
We send an email to you. Could you kindly click a link within that email?<br />

あなたの電子メール・アドレス宛てにメールを送信します。<br>
その電子メールの中にあるリンクをクリックして下さい。それによって、初めてあなた様の登録が完了します。</td></tr>
<tr><td class="b">** If you do not recieve our email within 1 hour, your email input may be wrong.
<br />** 1時間以内にメールが届かない場合、あなたの入力されたメール・アドレスが間違っている可能性があります。</td></tr>
<tr><td class="g">Or, your email might be distributed in your annoyance folder. Thus could you kindly check the annoyance folder as well?
<br />あるいは、送付しました 電子メールが迷惑メールとして扱われ、自動的に「迷惑メール・フォルダ」に
振り分けられている可能性もあります。従いまして、 「迷惑メール・フォルダ」もチェックして頂ければ幸いです。</td></tr>
</table>
</div>
</bodY>
</html>

<?php		// member_registration03.php

	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');	
	charSetUTF8();
	session_start();
	$today_year = date("Y");
/*********************************************************
	2014/07/16変更（はじめ）
*********************************************************/
//	print_r($_POST); echo "<br>"; 
/*********************************************************
	2014/07/16変更（おわり）
*********************************************************/

	if ((!isset($_SESSION['mem_reg'])||($_SESSION['mem_reg'] != hash('sha512',$today_year)))||
		!isset($_SESSION['auth_mem_reg'])||($_SESSION['auth_mem_reg'] != hash('sha512',$magic_code.$_POST['email']))) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}

//接続
 	try {
    // MySQLサーバへ接続
				$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
				$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる

	} catch(PDOException $e){
    	die($e->getMessage());
	}

	$stmt = $pdo->prepare("INSERT INTO `dr_tbl` (`dr_name`, `sirname`, `firstname`, `is_male`, `birth_year`, `hp_name`, `hp_address`, `country_code`, `job_kind`, `email`, ".
			"`dr_pwd`, `clue`, `hint`, `created`, `ip`, `dr_url`, `is_active`, `is_usable`, `is_deleted`) VALUES ".
			"(:dr_name, :sirname, :firstname, :is_male, :birth_year, :hp_name, :hp_address, :country_code, :job_kind, :email, :dr_pwd, :clue, :hint, :created, ".
			":ip, :dr_url, :is_active, :is_usable, :is_deleted)");
	$stmt->bindValue(":dr_name", $_POST['dr_name']);
	$stmt->bindValue(":sirname", $_POST['sirname']);
	$stmt->bindValue(":firstname", $_POST['firstname']);
	$stmt->bindValue(":is_male", $_POST['is_male']);
	$stmt->bindValue(":birth_year", $_POST['birth_year']."-01-01");
	$stmt->bindValue(":hp_name", $_POST['hp_name']);
	$stmt->bindValue(":hp_address", $_POST['hp_address']);
	$stmt->bindValue(":country_code", $_POST['country_code']);
	$stmt->bindValue(":job_kind", $_POST['job_kind']);
	$stmt->bindValue(":email", $_POST['email']);
	$stmt->bindValue(":dr_pwd", hash('sha512',$_POST['dr_pwd'].$magic_code));
	$stmt->bindValue(":clue", $_POST['clue']);
	$stmt->bindValue(":hint", hash('sha512',$_POST['hint'].$magic_code));
	$stmt->bindValue(":created", date('Y-m-d H:i:s'));
	$stmt->bindValue(":ip", $_SERVER['REMOTE_ADDR']);
	$stmt->bindValue(":dr_url", gethostbyaddr($_SERVER['REMOTE_ADDR']));
	$stmt->bindValue(":is_active", "0");
	$stmt->bindValue(":is_usable", "1");
	$stmt->bindValue(":is_deleted", "0");

	$stmt_chk = $pdo->prepare("SELECT * FROM `dr_tbl` WHERE `email` = :email");
	$stmt_chk->bindValue(":email", $_POST['email']);
	$stmt_chk->execute();
	if ($stmt_chk->rowCount() < 1) {	// この判定をしないと、この画面でrefreshされると何回もDB登録が発生する
		$stmt->execute();		// 仮登録された
		$subject = "${site_name} Registration Confirmation Mail (登録確認メール)";
		$sender = mb_encode_mimeheader("NPO TRI International Network");
		$headers  = "FROM: ".$sender."<$support_mail>\r\n";		
		$parameters = '-f'.$support_mail;	
		$md5 = substr(hash("sha512", $magic_code.$_SESSION['email']), 10, 32);
	
	//　ここからメール内容
	$body = <<< _EOT_
Dear Mr/Ms/Dr {$_POST['dr_name']}:
	
Thank you for your registration in NPO TRI International Network.
In order to confirm your email address ( = your login ID), could 
your kindly click the following URL?

この度は 特定非営利活動法人ティー・アール・アイ国際ネットワークへ登録ありがとうございます。
メール・アドレス(これはログインIDとなります)確認のため、下記のURLをクリックして下さい。
	
{$site_url}mem_reg/member_registration04.php?email={$_POST['email']}&md5={$md5}
Your email address: {$_POST['email']}
Your login ID : {$_POST['email']}
	
If you have any questions, could you contact to　{$support_mail}?

ご質問やご意見ありますれば　{$support_mail} まで
お問い合わせ下さい。
===========================================================
	$site_name
	$site_url
_EOT_;
	// _EOT_　は行の最初に記述し、;の後にコメントも無し、にしないとエラーとなる	

		if ($_SERVER['SERVER_NAME'] === 'localhost') {
			echo "<br>".$body."<br>";
		} else {
			mb_language('uni');
			mb_internal_encoding('utf-8');
			mb_send_mail($_POST['email'], $subject, $body, $headers, "-f$support_mail"); 
		}


		$_SESSION = array();
		session_destroy();
?>


<?php 
		exit();
	}
?>
